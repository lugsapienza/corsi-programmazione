"""
Un modulo è un file contenente istruzioni in linguaggio Python.
Questo file può essere eseguito come un programma assestante, mediante la modalità script, oppure può essere importato all'interno di un altro modulo.
In entrambi i casi verranno definiti i simboli: PI, sommank, fattoriale e fibonacci.
Al simbolo: __name__ l'interprete associa automaticamente una stringa contenente il valore: "__main__" in caso di esecuzione nella modalità script,
oppure il nome del modulo stesso, senza l'estensione, in caso di importazione.
Controllando il valore di tale simbolo è possibile capire in quale modalità il modulo è stato utilizzato, ed eseguire o meno determinate istruzioni.
"""

if __name__ == "__main__": print("Sto eseguendo il codice") #Questa print verrà eseguita solo in modalità script

#I seguenti simboli verranno definiti in entrambe le modalità d'utilizzo:
PI=3.14

def sommank(n,k): return n+k

def fattoriale(n):
	if n == 1: return n
	result = n * fattoriale(n-1)
	return result
def fibonacci(n):
	if n <= 1: return n
	return (fibonacci(n-1) + fibonacci(n-2))
