"""
Un modulo può essere importato all'interno di un altro che necessiti di utilizzarne i simboli.
E' possibile importare un modulo per intero, inserendone il simbolo corrispondente nella tabella del modulo corrente, in questo modo: 
import nomemodulo

Se si desidera importare selettivamente solo alcuni simboli definiti in un certo modulo:
from nomemodulo import simbolo1, simbolo2, ... ,simboloN

Per importare tutti i simboli di un modulo, in un altro, è sufficiente sostituire l'elenco con un asterisco:
from nomemodulo import *

Nel caso in cui venisse importato un simbolo già presente nel modulo corrente, l'oggetto ad esso associato verrà sostituito con quello importato.
"""

def sommank(n,k):return 2*n+k #Definizione della funzione: sommank
print("Prima esecuzione di sommank:",sommank(2,3)) #Esecuzione della funzione definita

from math import sommank  
#Importazione della funzione: sommank definita nel modulo math(avendo il suo stesso nome, sostituirà quella definita in precedenza)

print("Seconda esecuzione di sommank(dopo l'import):",sommank(2,3)) 
#Riesecuzione della funzione: sommank(verrà eseguita quella importata dal modulo: math, e non quella definita inizialmente).

