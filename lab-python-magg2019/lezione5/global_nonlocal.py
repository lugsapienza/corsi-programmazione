#ESEMPIO 1:
print("\n----Esempio 1----")

#max = 11
def f():
	#max = 22
	def f():
		#max = 88
		print("'max' in seconda f: ",max)
	f()
	print("'max' in prima f: ",max)
f()
print("'max' in Global: ",max)

"""
SPIEGAZIONE:


Modalità d'esecuzione:

Eseguire 4 volte questo esempio:
- La prima volta senza modificare nulla.
- La seconda volta, decommentando l'assegnazione: 'max = 11'
- La terza volta, decommentando anche l'assegnazione: 'max = 22'
- La quarta volta, decommentando anche l'assegnazione: 'max = 88'

Tale esempio mostra come l'interprete, alla comparsa di un simbolo all'interno del codice, ne cerchi il valore corrispondente prima nella tabella locale,
e poi, via via, in tutte le tabelle più esterne, fino ad arrivare alla tabella globale, ed in fine nella tabella del modulo: __builtins__.

Questo è lo stesso motivo per il quale è possibile definire funzioni annidate che hanno lo stesso nome di quelle che le contengono.
"""


#ESEMPIO 2:
print("\n----Esempio 2----")

sm="modulo"    #Definizione della stringa: sm, nel modulo(sm è globale): Esempio.py
def func1():    #Definizione della funzione func1, anch’essa globale
	sf="func1"    #Il simbolo sf è locale alla funzione func1
	def func2():    #La funzione func2 è locale alla funzione func1
		global sm    #Creazione riferimento al simbolo sm globale 
		sm = "modificata da func2"    #Modifica del simbolo globale: sm
		
		nonlocal sf    #Creazione riferimento al simbolo sf definito in func1
		sf = "modificata da func2"    #Modifica del simbolo sf di func1
		
	print("Func1 prima dell'esecuzione: ",sf)  
	func2()
	print("Func1 dopo l’esecuzione : ",sf)    #func2 ha modificato sf
	
print("Modulo prima dell'esecuzione : ",sm)
func1()    #func1 ha modificato sm, tramite func2
print("Modulo dopo l’esecuzione : ",sm)

"""
SPIEGAZIONE:

Questo secondo esempio mostra come sia possibile referenziare un simbolo presente nella tabella più esterna a quella locale,
oppure in quella globale, con lo scopo di modificarne il contenuto.
Per referenziare un simbolo presente in una tabella più esterna a quella locale è sufficiente specificare la parola: 'nonlocal' davanti al simbolo in questione.
Per referenziare un simbolo presente nella tabella globale(del modulo corrente) è sufficiente specificare la parola: 'global' davanti al simbolo in questione.
"""
