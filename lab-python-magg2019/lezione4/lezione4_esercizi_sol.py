#Es1:
def esegui():
	"""
	Definisce le funzioni:
		- differenza_liste(l1,l1)
		- mia_print(s)
	e le esegue in quest'ordine, stampandone a video i valori ritornati in output.
	La funzione: mia_print prende come parametro la conversione a stringa della lista ottenuta dalla funzione: differenza_liste.
	"""
	def differenza_liste(l1,l2):
		"""
		Prende in input due liste e ne ritorna la differenza (una NUOVA lista contenente gli elementi NON comuni).
		"""
		if not type(l1) == list or not type(l2) == list: return None
		l=[]
		for e in l1:
			if e not in l2: l+=[e]
		for e in l2:
			if e not in l1: l+=[e]
		return l

	def mia_print(s):
		"""
		Prende in input una stringa e la stampa a schermo, ritornandone, in output, il numero di caratteri.
		"""
		if not type(s) == str: return None
		print(s)
		return len(s)
		
	l1=[1,2,3,4]
	l2=[3,4,5,6]
	diff=differenza_liste(l1,l2)
	print("Liste in input:\n",l1,"\n",l2,"\nDifferenza: ",sep="")
	print("\nCaratteri stampati: ",mia_print(str(diff)))


#Es2:
def massimo(l):
	"""
	Prende in input una lista: l, NON vuota, di numeri Reali, e ne ritorna in output quello più grande.
	In caso di lista vuota, o di un oggetto di tipo differente, la funzione ritorna il valore nullo: None.
	In caso di oggetto differente da un numero intero o reale nella lista, la funzione ritorna il valore nullo: None.
	"""
	if not type(l) == list or not l: return None
	max=l[0]
	for e in l:
		if not type(e) == int and not type(e) == float: return None
		if e > max: max=e
	return max


#Es3:
def numero_palindromo(n):
	"""
	Dato un numero intero n in input, ritorna True se palindromo, False altrimenti.
	In caso di oggetto differente da un numero intero, la funzione ritorna il valore nullo: None.
	"""
	if not type(n) == int: return None
	num=n
	pal=0
	while num:
		pal = pal * 10 + num%10
		num //= 10
	if pal == n: return True
	return False


#Es4:
def genera_istogramma(l, simbolo="*"):
	"""
	Prende in input una lista di interi, ed un simbolo(stringa di 1 carattere) e restituisce una nuova stringa 
	contenente l'istogramma dei valori presenti in lista, disegnato utilizzando il simbolo specificato in input.
	
	'simbolo' è una stringa, di lunghezza 1, utilizzata per disegnare l'istogramma.
	Di default il suo valore è: "*".
	
	In caso di oggetto NON lista, lista vuota o elemento NON numerico intero presente in lista, 
	la funzione ritorna il valore nullo: None.
	"""
	if not type(l) == list or not l or not type(simbolo) == str or not len(simbolo) == 1: return None
	llen=len(l)
	ig=""
	for val in range(llen):
		if not type(l[val]) == int: return None
		ig += simbolo*l[val]
		if val < llen-1: ig+="\n\n"
	return ig
