"""
ESERCIZIO 1 'Stampa differenze':

Implementare la funzione: esegui(), che effettui una chiamata alla funzione: differenza_liste(l1, l2), 
ed alla funzione: mia_print(lista ritornata da: differenza_liste), dopo averle definite entrambe, 
stampandone a video i valori ritornati in output.

differenza_liste(l1,l2): Prende in input due liste e ne ritorna la differenza (una NUOVA lista contenente gli elementi NON comuni).
mia_print(s): Prende in input una stringa e la stampa a schermo, ritornandone, in output, il numero di caratteri.

NB: Alla sua chiamata, la funzione: mia_print dovrà prendere, come parametro, la conversione a stringa della lista ottenuta dalla funzione: differenza_liste.
Per convertite una list l, in stringa, è possibile utilizzare la funzione: str(l)



FIRME DELLE FUNZIONI:

esegui() -> None
	Definisce: differenza_liste(l1,l2) e mia_print(s), utilizzandole in serie.
differenza_liste(l1,l2) -> List
	Ritorna una nuova lista contenente gli elementi NON comuni tra l1 ed l2.
mia_print(s) -> int
	Stampa a video la stringa s e ne ritorna il numero di caratteri in output.

"""
def esegui():
	def differenza_liste(l1,l2):pass
	def mia_print(s):pass
	
	pass


"""
ESERCIZIO 2 'Massimo, il reale':

Implementare la funzione: massimo(l), che data in input una lista: l, NON vuota, di numeri Reali, ne ritorni in output quello più grande.
In caso di lista vuota, o di un oggetto di tipo differente, la funzione dovrà ritornare il valore nullo: None.
Nel caso in cui la lista dovesse contenere un oggetto differente da un numero reale, la funzione dovrà ritornare il valore nullo: None.
"""
def massimo(l):pass


"""
ESERCIZIO 3 'Il Palindromo': 

Un numero è palindromo quando le sue cifre, se scritte in una particolare base, rappresentano lo stesso valore sia che siano lette da destra che da sinistra.

Implementare la funzione: numero_palindromo(n), che dato in input un numero intero, ritorni True se tale numero è palindromo, False altrimenti.
Nel caso in cui l'oggetto passato non dovesse essere un intero, la funzione dovrà ritornare il valore nullo: None.
"""
def numero_palindromo(n):pass


"""
ESERCIZIO 4 'Istogram':

Implementare la funzione: genera_istogramma(l, simbolo), che data in input una lista di interi ed un simbolo(stringa di 1 carattere),
restituisca una nuova stringa contenente l'istogramma dei valori presenti in lista, disegnato utilizzando il simbolo specificato in input.

La funzione deve poter essere chiamata senza specificare il simbolo da utilizzare nell'istogramma; in tal caso la funzione adotterà il
simbolo di default: '*'
Nel caso in cui l'oggetto passato non dovesse essere una lista, la lista dovesse essere nulla, o dovesse contenere oggetti non di tipo intero,
la funzione dovrà ritornare il valore nullo: None.
"""
def genera_istogramma(l, simbolo):pass
