"""
Seconda variante esercizio sulla mini calcolatrice

In questa terza soluzione oltre ad utilizzare argv al posto della input(), per acquisire gli operandi da sommare,
è stato introdotto il controllo dei dati in input al programma, eseguito gestendo le eccezioni!

Invece di controllare che la stringa acquisita in input rappresenti un numero naturale, analizzandone il contenuto,
tale stringa viene passata direttamente alla funzione int, che in caso di stringa non valida lancerà un'eccezione
di tipo: ValueError.
Questo codice gestisce tale eccezione proteggendo le esecuzioni della funzione int tramite il costrutto: try-except
che cattura l'eventuale eccezione lanciata da una delle due chiamate a tale funzione, ed esegue uno
specifico blocco di codice che stampa un messaggio d'errore.

Per mezzo della: finally, sia in caso d'errore, che non, tale programma stamperà a video la stringa: "Programma terminato.", prima di terminare.


ESECUZIONE:

 python3 minicalc_param_eccezioni.py 2 3
"""

from sys import argv

if len(argv)!= 3: print("Usare in questo modo:\n   python3 input_par.py [operandoA] [operandoB]\n\n")
else:
	a = argv[1]
	b = argv[2]
	
	try:
		a = int(a) 
		#In caso di stringa errata, int() lancerà un'eccezione e l'esecuzione passerà direttamente al blocco except, 
		#saltando eventuali istruzioni precedenti
		
		b = int(b)
		#In caso di stringa errata, int() lancerà un'eccezione e l'esecuzione passerà direttamente al blocco except, 
		#saltando eventuali istruzioni precedenti
		
		print(a+b)
	except ValueError: print("\n   Uno dei due parametri inseriti NON e' un numero!\n\n")
	finally: print("Programma terminato.") #Esempio di utilizzo del finally (questa print viene eseguita sia in caso d'errore, che non).
