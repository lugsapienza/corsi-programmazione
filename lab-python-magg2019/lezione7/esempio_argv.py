"""
ESEMPIO PASSAGGIO PARAMETRI ALL'INTERPRETE

Questo moduo stampa a video, su righe separate, l'elenco dei parametri 
che sono stati passati all'interprete durante la sua chiamata.

Viene anche stampata la lista del tipo degli oggetti che sono stati
passati all'interprete (inseriti nella lista: argv).

Come visibile in quest'ultimo elenco, i parametri passati all'interprete 
sono SEMPRE di tipo stringa.


ESECUZIONE:
 python3 esempio_argv.py [parametro1] [parametro2]  [parametroN]

 python3 esempio_argv.py ciao mamma
 python3 esempio_argv.py "ciao mamma" 2
"""

from sys import argv

#Stampa elenco dei parametri passati all'interprete
pars=""
for e in argv: pars+=repr(e)+"\n"
print("\nArgomenti passati all'interprete nella lista argv:\n",pars)


#Stampa del tipo dei parametri passati all'interprete
tipi=""
for e in argv: tipi+=str(type(e))+"\n"
print("\nTipi di oggetti in argv:\n",tipi)
