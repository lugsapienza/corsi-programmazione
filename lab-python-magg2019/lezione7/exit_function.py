"""
La funzione: exit(), definita nel modulo: sys.
Utilizzata per terminare esplicitamente il programma, con un eventuale codice d'errore.

Questa funzione termina il programma lanciando un'eccezione di tipo: SystemExit

Gestendo questa eccezione è possibile annullare l'effetto della exit stessa, mantenendo il programma in esecuzione.


ESECUZIONE:

 python3 exit_function.py
"""


from sys import exit

try: exit()
except SystemExit as e: print("Codice di uscita: ",e) 
#La gestione di tale eccezione è utile quando si vuole ritardare o annullare, per un qualche motivo, la terminazione del programma.

#NB: L'espressione: 'SystemExit as e' viene utilizzata per associare un simbolo all'oggetto eccezione, 
#così da poterlo utilizzare all'interno dell'handler (in questo caso per stamparne la descrizione, che contiene l'eventuale codice d'errore della exit).


"""
E' possibile passare un parametro alla exit che, se di tipo intero, 
verrà ritornato al chiamante del programma(Terminale o Prompt dei comandi), 
come codice di terminazione(terminato senza o con errori), altrimenti verrà stampato a video.

Alcuni esempi:
"""
try: exit(0) 
except SystemExit as e: print("Codice di uscita: ",e) 
#La terminazione viene annullata, mostrando il codice d'errore (0 significa: Programma terminato correttamente).

try: exit([1,2,3])
except SystemExit as e: print("Codice di uscita: ",e)
#La terminazione viene annullata, stampando a video la conversione a stringa di una lista di interi
#(Qualsiasi valore, diverso da: 0, passato alla exit, assume il significato di: Programma terminato con errori).


try: exit("Fammi uscireee!!!")
except SystemExit as e: print("Codice di uscita: ",e)


exit("SONO USCITOOO..ma qualcosa e' andato storto.")
#In questo caso l'eccezione che la exit utilizza per terminare il programma, non è stata catturata.
#Il programma termina con codice d'errore: "SONO USCITOOO..ma qualcosa e' andato storto."

print("...altre istruzioni...") #Questa print non verrà mai eseguita.

"""
NB: Non confondere la funzione exit definita in sys, con la COSTANTE exit, predefinita (che non necessita di un import).
Quest'ultima ha la stessa firma della exit definita in sys, ma è corretto utilizzarla SOLO nella modalità interattiva!
"""
