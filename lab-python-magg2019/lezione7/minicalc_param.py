"""
ESERCITAZIONE SU ACQUISIZIONE DATI ALLA CHIAMATA DEL PROGRAMMA tramite argv


Prima variante esercizio sulla mini calcolatrice.

Scrivere un modulo che implementi una mini calcolatrice in grado di stampare
a video la somma dei due operandi numerici passatogli come parametro all'avvio.

In caso di parametri mancanti, il programma dovrà SOLO stampare a video una stringa
contenente le istruzioni di utilizzo.

Effettuare gli opportuni controlli per evitare che un utilizzo scorretto del programma
comporti la sua terminazione con errori.


ESECUZIONE:

 python3 minicalc_param.py 2 3
"""


#SOLUZIONE:

from sys import argv 
#I parametri passati all'interprete vengono resi disponibili, in formato stringa, 
#all'interno della lista: argv, definita nel modulo: sys

def isNumber(arg):	
	"""
	Prende come parametro una stringa: arg.
	Ritorna True se arg rappresenta un numero naturale, False altrimenti.
	"""
	l = ["0","1","2","3","4","5","6","7","8","9"]
	#ciclo su ogni carattere per controllare se è un numero tra 0 e 9
	for c in arg:
		if c not in l: return False
	return True


#Prima di accedere ai parametri desiderati, bisogna assicurarsi che quest'ultimi siano stati passati in input
#(per evitare un possibile accesso ad elementi di argv inesistenti):
if len(argv)!= 3: print("Usare in questo modo:\n   python3 input_par.py [operandoA] [operandoB]\n\n")
else:
	a = argv[1] #Accesso al primo operando d'interesse
	b = argv[2] #Accesso al secondo operando d'interesse
	
	#Controllo che le stringhe A e B rappresentino due numeri naturali, utilizzando la funzione: isNumber
	if not isNumber(a) or not isNumber(b): print("\n   Uno dei due parametri inseriti NON e' un numero!\n\n")
	else: print("\n Somma: ",int(a)+int(b),"\n\n") #Se lo sono, li converto ad intero e ne stampo la somma.
