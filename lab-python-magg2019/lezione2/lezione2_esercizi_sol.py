"""
ESERCIZIO 1 'La matrice delle occorrenze ':

Data una lista L di stringhe, creare una matrice M SIMMETRICA, contenente R righe quante sono le stringhe in L, 
e C colonne quanti sono i caratteri della stringa più lunga presente in L.
La matrice dovrà memorizzare, per i caratteri di ogni stringa, il conteggio delle loro occorrenze all’interno di essa, 
inserendo degli zeri per rendere la matrice simmetrica, nel caso in cui la stringa in esame dovesse avere meno caratteri rispetto a quella più lunga.


Esempio:

Lista: L = [“ieri”,”sono”,”stato”,”al”,”bagno”]
Risultato: M = [[2,1,1,2,0], [1,2,1,2,0], [1,2,1,2,1], [1,1,0,0,0], [1,1,1,1,1]]
"""

l = ["ieri","sono","stato","al","bagno"]

#----SOLUZIONE----
m = []
max = 0 
for s in range(len(l)):
	slen=len(l[s])
	if slen > max: 
		for e in range(s): m[e]+=[0]*(slen-max) #Torno indietro per aggiungere gli zeri mancanti
		max = slen
	row=[]
	for c in range(slen):
		row+=[0]
		for k in range(slen):
			if l[s][c] == l[s][k]: row[c]+=1
	if slen < max: row+=[0]*(max-slen) #Se la stringa corrente è più corta, devo aggiungere a lei gli zeri
	m+=[row]

"""
SPIEGAZIONE:

Questa soluzione prevede lo scorrimanto della lista 'l' e di ogni stringa in essa contenuta.
Ogni carattere viene confrontato con gli altri della medesima stringa e conteggiato in caso di uguaglianza.
Terminati i confronti per tale carattere, il conteggio ottenuto viene inserito in una lista,
che conterrà i conteggi di tutti i caratteri della stringa correntemente in esame.
Completata l'operzione su tutti i caratteri della stringa, tale lista viene aggiunta alla matrice 'M'.
Nel caso una stringa in esame dovesse contenere più caratteri di quelle esaminate in precedenza,
prima di procedere con la nuova stringa, verranno aggiunti gli zeri corrispondenti ai caratteri in meno, 
presenti sulla nuova stringa, nelle liste prodotte dalle stringhe precedentemente esaminate (per mantenere simmetrica la matrice risultante).
"""

#-----------------

print("Esercizio 1:\n",l,"\n\n",m,sep="")


"""
ESERCIZIO 2 'L’inverter':

Data una lista: L di stringhe, produrre una nuova lista: Z contenente tutte le stringhe di L, invertite.


Esempio:

Lista: L = [“Sono”,”Anna”]
Risultato: Z = [“onoS”,”annA”]
"""

l=["Sono","Anna"]
z=[]

#----SOLUZIONE----
for s in l:
	c=""
	for car in s: c=car+c #stringa = nuovoCarattere + stringa
	z+=[c] #Aggiunta stringa risultate a lista 

"""
SPIEGAZIONE:

Questa soluzione prevede lo scorrimento della lista: 'l' e di ogni stringa in essa contenuta.
Ogni carattere viene accumulato anteriormente al precedente, in una variabile di appoggio,
così da ottenere la stringa invertita, che poi viene aggiunta nella lista: 'Z' risultante.
"""

#-----------------

print("\nEsercizio 2:\n",l,"\n\n",z,sep="")


"""
ESERCIZIO 3 'Il collassatore delle nullità':

Data una lista 'L' eterogenea, iterare su di essa rimpiazzando eventuali elementi nulli con UNA COPIA della lista stessa.
In caso di liste annidate contenenti anch’esse valori nulli, rimpiazzarli con la copia della rispettiva LISTA DI APPARTENENZA.


Esempio:

Lista iniziale: L = [1,2,["ciao",""],0]
Lista modificata: [1,2,["ciao",["ciao",""]],[1,2,["ciao",["ciao",""]],0]]
"""

l = [0,["",56],1] #Prova anche con: [1,2,["ciao",""],0]

print("\nEsercizio 3:\n",l,sep="")

#----SOLUZIONE----
llen=len(l)
lists=[[l,0,llen]] #Elenco liste da scorrere(inizialmente solo quella principale), con relative info: [lista, indice prox elemento da esaminare, lunghezza].
ref=0
idx=0
while idx<llen:
	if not lists[ref][0][idx]: lists[ref][0][idx] = lists[ref][0][:] #Se l'idx-esimo valore della lista: lists[ref][0] è nullo, sostituiscilo con la copia della lista
	elif type(lists[ref][0][idx]) == list: #Se l'idx-esimo valore della lista: lists[ref][0] è una lista, scorrila reimpostando idx, llen e ref. 
		lists[ref][1]=idx+1 #Salvataggio indice del prossimo elemento da esaminare nella lista corrente, prima di cambiarla(per riprendere da li, successivamente).
		llen=len(lists[ref][0][idx])
		lists+=[[lists[ref][0][idx],0,llen]] #Aggiunta nuova lista trovata, all'elenco delle liste da scorrere.
		ref+=1 #Selezione della nuova lista da esaminare(quella appena trovata).
		idx=-1 #Reset indice degli elementi (a: -1 per via dell'incremento nella riga successiva).
	idx+=1
	if idx == llen and ref: #Se terminata la lista corrente, riprendi a scorrere quella iniziata in precedenza.
		lists=lists[:ref]
		ref-=1
		idx=lists[ref][1]
		llen=lists[ref][2]

"""
SPIEGAZIONE:

In questo problema la cosa più impegnativa da fare è quella di spostarsi da una lista all'altra,
eseguendo, per ognuna:
- Sostituzione dei valori nulli con la copia della lista in esame.
- Scorrimento liste annidate.

Per permettere l'esecuzione di tali operazioni, in modo ricorsivo su eventuali liste annidate,
è stato utilizzato un ciclo while (invece di un for) e sono stati introdotti i seguenti simboli:
- llen -> Mantiene la lunghezza della lista correntemente in esame; utilizzato nella condizione di terminazione del while.

- lists -> Mantiene i riferimenti a tutte le liste che devono essere esaminate (sia quella iniziale, che eventuali liste annidate).
Di ogni lista vengono momorizzati anche: la posizione del prossimo elemento da esaminare e la lunghezza di quest'ultima.
Queste tre informazioni(lista, posizione, lunghezza), vegono memorizzate, in 'lists', all'interno di una lista dedicata(sono informazioni correlate).

- ref -> Mantiene l'indice della lista correntemente in esame.
- idx -> Mantiene l'indice dell'elemento correntemente in esame, nella lista corrente.

L'obbiettivo è quello di scorrere ogni lista, partendo da quella principale, sostituendo ogni elemento nullo con la copia della lista stessa.
Nel caso in cui un elemento fosse, a sua volta, una lista, anche in quest'ultimo i valori nulli dovranno essere sostituiti con una sua copia.
Lo scorrimento inizia con la lista principale, in caso di lista, non vuota, annidata:
- L'informazione sull'indice successivo a quello in esame, nella lista corrente, viene salvato in 'lists'(nella sottolista corrispondente alla lista in esame).
- La lista annidata viene aggiunta a 'lists', in una nuova tripla contenente: il suo riferimento, l'indice del prossimo elemento(0) e la sua lunghezza.
- 'idx' e 'llen' vengono azzerati per scorrere la nuova lista.
Ad ogni iterazione del ciclo, 'idx' (che mantiene il riferimento all'i-esimo elemento della lista corrente) viene incrementato di 1.
Al completamento di una lista, 'idx', 'llen' e 'ref' vengono impostati con le informazioni della lista precedente, in 'lists'.
Fatto ciò, la lista corrente viene rimossa da 'lists'.

Viene utilizzato un while per rendere possibile il reset o il ripristino di 'idx' ed 'llen', senza dover interrompere il ciclo
(cosa impossibile se utilizzato un for).
"""

#-----------------

print("\n",l,sep="")
			
