'''
EDITOR DI TESTO

Scrivere un programma che funzioni come un editor di test a riga di comando, dotato delle seguenti funzionalità:
- Inserimento di un testo su più righe (con il tasto: INVIO, si andrà a capo).
- Salvataggio, su richiesta (alla pressione della combinazione di tasti: Ctrl+C), del nuovo testo inserito, su un file (mantenendo l'eventuale contenuto preesistente).
- Chiusura del programma, con salvataggio automatico delle modifiche, alla pressione della combinazione di tasti: Ctrl+Z


Esempio di codice:

while True:
    try: input(": ")
    except KeyboardInterrupt: print("  <Salvato>\n")
    except EOFError:
            print("\n  <Salvato>\n<Esco...>\n")
            break


ESEMPIO D'ESECUZIONE:

$ python3 textEditor.py miofile.txt

: Ciao

: Mi chiamo Davide e questo è il Laboratorio di Python.

: Tutto chiaro?

: ^C
    > Saved

: Si?

: ^C
    > Saved

: O no?

: ^D
    > Saved
    > Quitting...


'''


#SOLUZIONE:

def save_file(data, file, closefile=False, mode="a"):
	"""
	data -> string/bytes
	file -> string/IOBase
	closefile -> boolean
	mode -> string
	
	Scrive l'oggetto: 'data' nel file: 'file', ritornando una tupla contenente:
	- L'oggetto corrispondente al file che è stato aperto.
	- Il numero di byte o di caratteri(in base alla modalità) che sono stati scritti.
	
	Se 'file' è di tipo stringa, esegue l'apertura del file.
	Se 'file' è di tipo: IOBase, utilizza il file aperto in precedenza.
	Al passaggio di altri tipi di oggetti a 'file', lancia l'eccezione: TypeError.
	Se 'data' è di tipo stringa e 'mode' non contiene il flag: 'b', scrive la stringa nel file; altrimenti lancia l'eccezione: ValueError.
	Se 'data' è di tipo bytes e mode contiene il flag: 'b', scrive la stringa nel file; altrimenti lancia l'eccezione: ValueError.
	Se 'closefile' non è un booleano o 'mode' non è una stringa, lancia l'eccezione: TypeError.
	Se 'mode' non contiene uno dei flag di scrittura, lancia l'eccezione: ValueError.
	"""
	#Questa funzione permette di salvare una stringa o una sequenza di byte all'interno di un file
	
	from io import IOBase #importazione del simbolo 'IOBase', per l'utilizzo nei controlli del parametro: file
	
	if type(closefile) != bool or type(mode) != str or not mode: 
		raise TypeError("'closefile' must be a boolean and 'mode' must be a string")
	if not "w" in mode and not "a" in mode and not "+" in mode and not "x" in mode: #la funzione esegue una write perciò deve avere il permesso di scrivere sul file
		raise ValueError("'mode' must contain one of the writing mode flags")
		
	if (not type(data) is bytes) and (not type(data) is str): 
		raise TypeError("'data' must be a bytes-like object or string object")
	elif (type(data) is bytes and not "b" in mode) or (type(data) is str and "b" in mode):
		raise ValueError("With a string object in 'data' field, 'mode' cannot contain the 'b' flag.\nWith a bytes-like object in 'data' field, 'mode' must contain the 'b' flag")
	
	if type(file) == str and file:
		try: file = open(file,mode)
		except OSError: raise
	elif not issubclass(type(file),IOBase): raise TypeError("'file' must be a string or file object")
	#issubclass() ci permette di controllare se la classe di un oggetto è figlia di una certa classe. 
	
	wrote = file.write(data)
	file.flush()
	
	if closefile: file.close()
	return file, wrote


def list_to_string(l, sep="\n"):
	"""
	l -> lista
	sep -> stringa t.c. len(stringa) = 1
	
	Prende in input una lista: "l" ed un separatore: "sep" e ritorna una stringa contenente tutti gli elementi di 'l', separati da 'sep'.
	In caso di parametri errati la funzione lancerà un: ValueError.
	"""
	
	if not type(l) == list or not type(sep) == str or len(sep)!=1: raise ValueError("'l' must be a list and 'sep' must be a single character string")
	string=""
	llen=len(l)
	for i in range(llen): 
		string+=str(l[i])
		if i < llen-1: string+=sep
	return string


def main():
	from sys import argv, exit
	l = []
	isopen = False
	filedata = None
	insertedlines = 0
	savedlines = 0
	
	if len(argv)<2: exit("Usage:\n    python3 textEditor.py [filename]") #Esco ritornando una stringa d'errore al chiamante
	while True:
		try:
			line = input("\n: ")
			l+=[line]
			insertedlines += 1
		except KeyboardInterrupt:
			try:
				if not isopen:
					isopen = True
					filedata = save_file(list_to_string(l[savedlines:insertedlines]), argv[1])
				else: save_file(list_to_string(l[savedlines:insertedlines]), filedata[0])
				savedlines = insertedlines
				print("\n    > Saved")
			except OSError: print("\n    > Opening file error")
			except Exception as e: print("\n    > Error: ", e)
		except EOFError:
			try:
				if not isopen:
					isopen = True
					filedata = save_file(list_to_string(l[savedlines:insertedlines]), argv[1], closefile=True)
				else: save_file(list_to_string(l[savedlines:insertedlines]), filedata[0], closefile=True)
				savedlines = insertedlines
			except OSError: print("\n    > Opening file error")
			except Exception as e: print("\n    > Errore: ", e)
			print("\n    > Saved\n    > Quitting...\n")
			break
			
if __name__ == "__main__":
    main()
