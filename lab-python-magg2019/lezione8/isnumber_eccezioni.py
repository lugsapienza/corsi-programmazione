"""
ESEMPIO DI LANCIO DI UN'ECCEZIONE


Utilizzo:
$python3

>>> import isnumber_eccezioni as exc
>>> exc.isNumber(1)
"""

def isNumber(arg):	
	"""
	Prende come parametro una stringa: arg.
	Ritorna True se arg rappresenta un numero naturale, False altrimenti.
	"""
	if not type(arg) == str: raise ValueError("arg non è una stringa")
	
	l = ["0","1","2","3","4","5","6","7","8","9"]
	#ciclo su ogni carattere per controllare se è un numero tra 0 e 9
	for c in arg:
		if c not in l: return False
	return True

"""
Questo esempio mostra uno dei contesti più opportuni per l'utilizzo delle eccezioni,
cioè nei controlli dei parametri di input nelle funzioni.

Quando si verifica un errore di qualche tipo, all'interno di un blocco di codice,
si crea il problema di come gestirlo e, nel caso di funzionalità sviluppate da persone
diverse, di come segnalarlo ad un altro blocco di codice.

Il caso più frequente è quello delle funzioni: Una funzione può andare in errore a causa, spesso,
di un suo cattivo utilizzo(un parametro errato o mancante può facilmente creare problemi),
ma quale formato o tipo di dato utilizzare per far capire al chiamante di una funzione, 
che quest'ultima è andata in errore?
E come differenziare un errore da un risultato corretto o nullo?

Si potrebbe utilizzare il None, ma questo oggetto non denota un errore, bensì un VALORE NULLO! 

E' necessario utilizzare un tipo di oggetto la quale semantica denoti un errore/un problema.
Le eccezioni servono proprio a questo, facendo anch'esse parte dei costrutti del linguaggio, non ci sarà bisogno di conoscere
l'implementazione di una certa funzione, per capire se questa sia andata in errore oppure no.

Al più sarà necessario leggerne la documentazione, per avere maggiori informazioni sul problema.
"""
