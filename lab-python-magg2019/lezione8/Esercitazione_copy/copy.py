"""
ESERCITAZIONE SULLA COPIA DI UN FILE: 'Python cp'

Scrivere un modulo: copy.py, che dato in input il nome di un file sorgente: A
ed il nome di un file di destinazione: B, copi il contenuto del file A nel file B.

DETTAGLI:
- Il file A deve già esistere prima dell'avvio del programma(non si può copire un file inesistente).
- Se il file: B dovesse già esistere, verrà sovrascritto con il contenuto del file: A.
- Se il file: B NON dovesse esistere, dovrà essere creato.
- In caso di mancato o errato passaggio dei parametri, il programma dovrà terminare mostrando a video le istruzioni sul corretto utilizzo.
- In caso di errori nelle operazioni sui file, Il programma dovrà terminare mostrando a video una descrizione dell'errore avvenuto.
- Assumere inizialmente che il file A sia di tipo testuale.
- Aggiungere successivamente il supporto per altri tipi di file(immagini, audio, video, ecc.).


ESECUZIONE:

python3 copy.py [nomeFileA] [nomeFileB]
"""


#SOLUZIONE:

from sys import argv, exit


#Controllo parametri di input:

if len(argv) != 3: exit("\n\nUtilizzo: python3 copy.py [nomeFileSorgente] [NomeFileDestinazione]\n") 
#La exit può prendere come parametro una stringa, che verrà stampata a video prima della terminazione (descrizione dell'errore avvenuto)


#Apertura dei file e copia di TUTTO il contenuto del file sorgente, nel file di destinazione:

source = None
destination = None
data = None

try: source = open(argv[1],"rb")    #La open può lanciare delle eccezioni, che vanno gestite
except OSError as error: exit("\n\n Errore nell'apertura del file sorgente: "+str(error)+"\n")

try: data = source.read()
except Exception as error: exit("\n\n Errore nella lettura del file sorgente: "+str(error)+"\n")
finally: source.close()


try: destination = open(argv[2],"wb")
except OSError as error: exit("\n\n Errore nell'apertura del file destinazione: "+str(error)+"\n")

try: destination.write(data)
except Exception as error: exit("\n\n Errore nella scrittura del file destinazione: "+str(error)+"\n")
finally: destination.close()



"""
SPIEGAZIONE:

Questa soluzione prevede la lettura completa del file sorgente 
e la successiva scrittura completa nel file di destinazione.

La comparsa di eventuali errori nell'apertura di un file, nella lettura 
o nella scrittura, è stata gestita inserendo tali istruzioni in un blocco try-except.

Tale organizzazione del codice(prima tutte le operazioni sul file sorgente,
e poi tutte quelle sul file di destinazione) permette l'utilizzo della finally
per le operazioni di chiusura dei file, sia in caso d'errore, che non.
"""
