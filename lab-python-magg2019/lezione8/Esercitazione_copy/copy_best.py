"""
ESERCITAZIONE SULLA COPIA DI UN FILE: 'Python cp' - Soluzione con risparmio di memoria

Scrivere un modulo: copy.py, che dato in input il nome di un file sorgente: A
ed il nome di un file di destinazione: B, copi il contenuto del file A nel file B.

DETTAGLI:
- Il file A deve già esistere prima dell'avvio del programma(non si può copire un file inesistente).
- Se il file: B dovesse già esistere, verrà sovrascritto con il contenuto del file: A.
- Se il file: B NON dovesse esistere, dovrà essere creato.
- In caso di mancato o errato passaggio dei parametri, il programma dovrà terminare mostrando a video le istruzioni sul corretto utilizzo.
- In caso di errori nelle operazioni sui file, Il programma dovrà terminare mostrando a video una descrizione dell'errore avvenuto.
- Assumere inizialmente che il file A sia di tipo testuale.
- Aggiungere successivamente il supporto per altri tipi di file(immagini, audio, video, ecc.).


ESECUZIONE:

python3 copy.py [nomeFileA] [nomeFileB]
"""


#SOLUZIONE:

from sys import argv, exit
from io import DEFAULT_BUFFER_SIZE   #INFO: https://docs.python.org/3/library/functions.html#open

debug = False   #Imposta a: True, per visualizzare i vari step della copia



#Controllo parametri di input:

if len(argv) != 3: exit("\n\nUtilizzo: python3 copy.py [nomeFileSorgente] [NomeFileDestinazione]\n") 
#La exit può prendere come parametro una stringa, che verrà stampata a video prima della terminazione (descrizione dell'errore avvenuto)



#Apertura dei file:

source = None
destination = None

try: source = open(argv[1],"rb")    #La open può lanciare delle eccezioni, che vanno gestite
except OSError as error: exit("\n\n Errore nell'apertura del file sorgente: "+str(error)+"\n")

try: destination = open(argv[2],"wb")
except OSError as error:
	source.close()    #Prima di terminare, bisogna rilasciare la risorsa(file), precedentemente acquisita
	exit("\n\n Errore nell'apertura del file destinazione: "+str(error)+"\n")



#Lettura, a blocchi di lunghezza: DEFAULT_BUFFER_SIZE, del file sorgente, e scrittura nel file di destinazione:

data = "\n"    #Serve per entrare nel ciclo sottostante(se fosse una stringa vuota, non ci entrerebbe mai)
while data:
	
	#---DEBUG---
	if debug:
		print("\n----------\n   LAST READ DATA BLOCK: \"",data,"\"")
		input("\n   <PRESS 'INVIO' TO COPY NEXT BLOCK>")
	#---DEBUG---
	
	
	#Lettura blocco di DEFAULT_BUFFER_SIZE byte da source:
	
	try: data = source.read(DEFAULT_BUFFER_SIZE)
	except Exception as error:
		source.close()
		destination.close()
		exit("\n\n Errore nella lettura del file sorgente: "+str(error)+"\n")
	
	
	#Scrittura del blocco letto in precedena, sul file destination (eseguita fin quando tutti i byte non sono stati scritti):
	
	size = len(data)
	written = 0
	while written < size:
		
		#---DEBUG---
		if debug:
			print("\n\n   DIM:",size,"\n   WRITTEN:",written,"\n   DATA TO WRITE: \"",data[written:],"\"\n----------")
		#---DEBUG---
		
		try: written += destination.write(data[written:])
		except Exception as error:
			source.close()
			destination.close()
			exit("\n\n Errore nella scrittura del file destinazione: "+str(error)+"\n")
		destination.flush()

#---DEBUG---
if debug:
	print("\n\n-----AT THE END-----\nLAST READ DATA BLOCK: \"",data,"\"\nDIM:",size,"\nWRITTEN:",written,"\nLAST DATA TO WRITE: \"",data[written:],"\"\n")
#---DEBUG---

source.close()
destination.close()



"""
SPIEGAZIONE:

In questa soluzione, invece di essere letto e successivamente scritto per intero, il file sorgente viene letto e scritto a blocchi 
di dimensione pari a quella definita dal simbolo: DEFAULT_BUFFER_SIZE, del modulo standard: io.
Per ulteriori dettagli su questo simbolo, visitare la pagina: https://docs.python.org/3/library/functions.html#open

Tale approccio serve ad evitare che file di grandi dimensioni possano creare problemi di esaurimento della memoria
(un file troppo grande potrebbe non entrare, per intero, in memoria).

Per ogni nuovo blocco, letto dal file sorgente, il programma potrebbe necessitare di più operazioni di scrittura per salvarlo nel file di destinazione
(sempre a causa di una possibile mancanza di memoria).

La funzione: write() ritorna il numero di byte che sono stati scritti nel file con successo.
E' buona norma eseguire un controllo sul valore ritornato dalla write, in modo tale da ripetere la scrittura per eventuali byte mancanti.

Alla lettura di un blocco NULLO dal file sorgente, il programma termina uscendo dal ciclo e chiudendo i file.

Per proteggere l'esecuzione da eventuali errori di apertura, lettura o scrittura dei file,
tali operazioni sono state inserite in blocchi try-except che, in caso d'errore, prevedono la chiusura dei file aperti e la terminazione del programma.

Per osservare come ciascun blocco viene copiato, sono state aggiunte delle print di debug, ed una chiamata alla funzione: input,
utilizzata per bloccare l'esecuzione prima della copia di un nuovo blocco(il risultato della input non è d'interesse e perciò non viene salvato).

Per attivare la modalità debug è sufficiente assegnare il valore: True, al simbolo: debug, definito inizialmente.

NB: Al contrario dell'altra soluzione, qui non è possibile inserire la chiusura dei file in un blocco: finally.
"""
