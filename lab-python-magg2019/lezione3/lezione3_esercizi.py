#Es.1
def getElencoVocaboli(testo):
	"""
	Prende in input una stringa: 'testo' contenente parole separate da spazi, e ritorna una lista contenente tali parole.
	"""
	pass

"""
ESECUZIONI:

>>> from lezione3_esercizi import getElencoVocaboli

>>> getElencoVocaboli("  ")
[]
>>> getElencoVocaboli("")
[]
>>> getElencoVocaboli("ciao mamma")
['ciao', 'mamma']
>>> getElencoVocaboli("ciao   mamma")
['ciao', 'mamma']
>>> getElencoVocaboli("ciao   mamma  ")
['ciao', 'mamma']
>>> getElencoVocaboli(" ciao   mamma  ")
['ciao', 'mamma']
>>> getElencoVocaboli(" ciao   mamma  miamai")
['ciao', 'mamma', 'miamai']
>>> getElencoVocaboli(" ciao   mamma  miamai   ")
['ciao', 'mamma', 'miamai']
"""

#Es.2
def rimpiazza(testo, vecchias, nuovas):
	"""
	Prende in input 3 stringhe: testo, vecchias e nuovas, e ritorna una lista contenente:
	- Come primo elemento, una stringa ottenuta da 'testo', sostituendo tutte le occorrenze di 'vecchias' con 'nuovas'.
	- Come secondo elemento, un intero corrispondente al numero di occorrenze che sono state sostituite.
	- Come terzo elemento, l'elenco degl'indici dove sono state trovate le occorrenze (l'indice della prima lettera di ogni occorrenza).
	Se 'vecchias' NON dovesse comparire in testo, ritornerà solo la stringa originale(anche se vuota).
	"""
	pass

"""
ESECUZIONI:

>>> from lezione3_esercizi import rimpiazza

>>> rimpiazza("","motte","sera")
''
>>> rimpiazza("stanotte sarà una notte bellissima","motte","sera")
'stanotte sarà una notte bellissima'
>>> rimpiazza("stanotte sarà una notte bellissima","notte","sera")
['stasera sarà una sera bellissima', 2, [3, 17]]
>>> rimpiazza("stanotte sarà una notte bellissima notte nottenotte","notte","sera")
['stasera sarà una sera bellissima sera serasera', 5, [3, 17, 33, 38, 42]]
"""

#Es.3
def strENum(s):
	"""
	Prende in input una stringa: 's' contenente parole e numeri(solo interi) separati da spazi, e ritorna una lista contenente:
	- Come primo elemento, la lista delle sole parole(sottostringhe) contenute nella stringa; lista vuota se non presenti.
	- Come secondo elemento, una lista dei soli numeri interi(sottostringhe convertite ad intero) presenti nella stringa; lista vuota se non presenti.
	- Se 's' è vuota, la funzione restituirà una lista vuota (e non una lista di liste vuote).
	"""
	pass

"""
ESECUZIONI:

>>> from lezione3_esercizi import strENum

>>> strENum("123ciao234 123 ")
[[], [234, 123]]
>>> strENum("123ciao 123 ")
[['123ciao'], [123]]
>>> strENum("123ciao234 123 ")
[[], [234, 123]]
>>> strENum("123ciao 123 ")
[['123ciao'], [123]]
>>> strENum("ciao 123 ")
[['ciao'], [123]]
>>> strENum("ciao 123 6")
[['ciao'], [123, 6]]
>>> strENum("ciao 123 6 99")
[['ciao'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 k8i")
[['ciao', '8i'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 ki")
[['ciao', 'ki'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 k8")
[['ciao'], [123, 6, 99, 8]]
>>> strENum("ciao 123 6 99 k8 msca")
[['ciao', 'msca'], [123, 6, 99, 8]]
>>> strENum("  ciao  123 6   99 k8 msca   ")
[['ciao', 'msca'], [123, 6, 99, 8]]
"""

#Es.4
def alteraLista(dest, sorg, da, a):
	"""
	Prende in input 2 liste: 'dest', 'sorg' e due interi 'DA', 'A', e ritorna una NUOVA lista ottenuta da 'dest', sostituendo gli elementi dall'indice 'DA', all'indice 'A', con tutti quelli presenti in 'sorg'.
	Gli elementi nell'intervallo 'DA'-'A' saranno eliminati ed al loro posto ci saranno tutti gli elementi contenuti in 'sorg'.
	"""
	pass

"""
ESECUZIONI:

>>> from lezione3_esercizi import alteraLista

>>> alteraLista([1,2,3],["Bianco",True],1,2)
[1, 'Bianco', True, 3]
>>> int in [1,2,"ciao"]
False
>>> int in [1,2,"ciao",int]
True
"""

#Es.5
def filtraERaggruppa(tipi, ogg):
	"""
	Prende in input 2 liste: 
	- 'tipi': contenente un elenco di tipi (int, float, str, list, ecc..).
	- 'ogg': contenente oggetti di varie tipologie.
	Ritorna una NUOVA lista contenente gli oggetti in 'ogg', ordinati per tipologia, secondo l'ordine in cui, tali tipologie, compaiono in 'tipi'.
	Se in 'ogg' sono presenti oggetti di una tipologia non elencata in 'tipi', quest'ultimi NON compariranno nella lista risultante.
	"""
	pass
	
"""
ESECUZIONI:

>>> from lezione3_esercizi import filtraERaggruppa

>>> filtraERaggruppa([str,int,list,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
['ciao', 1, 2, 4, 5, 18, [1, 2], True, False]
>>> filtraERaggruppa([int,list,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, [1, 2], True, False]
>>> filtraERaggruppa([int,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, True, False]
>>> filtraERaggruppa([int,bool,int],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, True, False, 1, 2, 4, 5, 18]
>>> filtraERaggruppa([bool,int],[1,2,"ciao",4,True,5,False,18,[1,2]])
[True, False, 1, 2, 4, 5, 18]
"""
