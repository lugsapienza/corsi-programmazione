"""
Una funzione è un blocco di codice che implementa la logica 
di una qualche funzionalità prevista in un dato programma.

Una funzione scritta correttamente rispetta i principi di modularità del software,
rendendone possibile l'utilizzo in programmi differenti, senza necessità di alcuna modifica.

Tale blocco di codice deve poter essere utilizzato senza necessariamente conoscerne l'implementazione,
ma solo i dati da fornire in input o quelli ritornati in output.
A tale scopo è utile dotare ogni funzione di una documentazione che ne illustri le modalità di utilizzo.

                ____________
  DATI INPUT   |            | DATI OUTPUT
-------------->|  funzione  |--------------> 
               |____________|


ESEMPIO:

          ____________
  n,k    |            | n+k
-------->|  somma_n   |--------> 
         |____________|





ESEGUIRE IN QUESTO MODO: python3 15052019.py


NOTA: Per provare gli esempi illustrati di seguito, 
      si consiglia di commentare tutti gli altri, 
      una volta scelto quello che si vuole eseguire.
      
"""


#----------ESEMPIO 1:

def f():
	"""
	Non prende nessun parametro in input e non ritorna nessun valore.
	Stampa la stringa: "ciao".
	"""
	#Se inserita subito dopo la firma di una funzione, una stringa multi-riga viene interpretata come la sua Documentazione.
	#Utile per descriverne il funzionamento e l'utilizzo (tipi di parametri in input e valori in output).
	
	print("ciao")
	return        #Questa istruzione ritorna il controllo al chiamante della funzione (che in questo caso è il file 15052019.py, anche detto: "Modulo").
	print("cara") #Questa istruzione non verrà mai eseguita (la funzione ritorna prima della sua esecuzione).


help(f)           #La funzione help() permette di visualizzare la stringa di documentazione associata ad una funzione. 
print(f())        #Chiamata della funzione f() e stampa del valore ritornato da essa(Stampa il valore nullo: None, perché f non ritorna nessun valore).
print("Mamma")

print(f("ciao"))  #Questa chiamata genera un errore perché è stato passato un parametro stringa ad una funzione che non prende parametri in input.

#----------ESEMPIO 2:

def f():
	""" 
	Non prende nessun parametro in input e non ritorna nessun valore.
	Stampa la stringa: "ciao" e genera un ciclo infinito.
	"""
	print("ciao")
	while True: pass

#Se non viene inserito il return, la funzione ritornerà automaticamente dopo l'esecuzione di tutte le istruzioni in essa contenute.
#In tale caso, il valore ritoranto sarà quello di default (Il valore nullo: None).

print(f())
print("Mamma")     #Questa stringa non verrà mai stampata, a causa del fatto che la funzione f() non ritornerà mai.

#----------ESEMPIO 3:

#Definizione di una funzione della quale si conosce solo la semantica, ma non l'implementazione(si sa ciò che dovrà fare, ma NON COME dovrà farlo):

def miafunzionalita():pass  #La parola chiave: 'pass' viene utilizzata per segnalare una FUTURA implementazione della funzione.


miafunzionalita()           #Questa chiamata a funzione non genera errori

#----------ESEMPIO 4:

def somma_n(s,k):
	"""
	Funzione che ritorna la somma di due oggetti s e k.
	"""
	return s+k


print(somma_n())                   #Questa chiamata genera un errore di mancato passaggio dei due parametri richiesti dalla funzione.
print(somma_n(2,3))
print(somma_n("Ciao"," mamma"))
print(somma_n("ciao",1234))        #Non è possibile sommare una stringa con un intero, ma è possibile passarli come parametro! (E' quindi opportuno inserire dei controlli)
print(somma_n(["Ciao"],[1,2,3,4])) #La somma tra due liste è invice possibile e darà vita ad una nuova lista contenente gli elementi di entrambe.

#----------ESEMPIO 5:

def accedi(seq,pos):
	"""
	Funzione che ritorna l'oggetto alla posizione: pos, della sequenza: seq.
	"""
	return seq[pos]


print(accedi("ciao",0))
print(accedi(["ciao",22]*3,4)) #Le espressioni vengono valutate prima di chiamare la funzione e solo il risultato verrà passato come parametro
print(accedi(0,0))             #Lo 0 non è un oggetto sequenza; il suo passaggio a questa funzione genera un errore di tipo. 
print(accedi("ciao",2%2))      #2%2 viene sostituito con il suo risultato: 0


#----------ESERCITAZIONE 1:
"""
Scrivere una funzione che prenda in input due simboli: n e k.
Se n e k sono di tipo intero, ne ritorni la somma.
Altrimenti ritorni: None.
"""

#SOLUZIONE:
def somma_numeri(n,k):
	"""
	Prende in input due numeri interi n, k e ne ritorna la somma.
	Se n o k non dovessero essere di tipo intero, la funzione ritornerà: None.
	"""
	if type(n)==int and type(k)==int:   #Se n e k non sono interi, la funzione non deve fare nulla.
		return n+k
	else:
		return


print(somma_numeri("a",1))
print(somma_numeri(1,"a"))
print(somma_numeri("a","a"))
print(somma_numeri(1,1))     #Solo in questo caso la funzione eseguirà la somma richiesta.


#----------ESERCITAZIONE 1(VARIANTE):
"""
Scrivere una funzione che prenda in input due simboli: n e k.
Se n e k sono di tipo intero, ne ritorni la somma.
Altrimenti ritorni una lista contenente il tipo di n e quello di k.
"""

#SOLUZIONE:
def somma_numeri(n,k):
	"""
	Prende in input due numeri interi n, k e ne ritorna la somma.
	Se n o k non dovessero essere di tipo intero, la funzione ritornerà una lista contenente il tipo di n, e quello di k.
	"""
	if type(n)==int and type(k)==int:
		return n+k
	else:
		return [type(n),type(k)]  #Viene ritornato il tipo di ciascun parametro in input.


print(somma_numeri("a",1))
print(somma_numeri(1,"a"))
print(somma_numeri("a","a"))
print(somma_numeri(1,1))
