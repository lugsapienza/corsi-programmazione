#Es.1
def getElencoVocaboli(testo):
	"""
	Prende in input una stringa: 'testo' contenente parole separate da spazi, e ritorna una lista contenente tali parole.
	"""
	l=[""]
	i=0
	for p in testo:
		if not p == " ":l[i]+=p
		else:
			l=l+[""]
			i+=1
	return l

#Non scarta le sottostringhe vuote in alcun modo 
def getElencoVocaboli2(testo):
	"""
	Prende in input una stringa: 'testo' contenente parole separate da spazi, e ritorna una lista contenente tali parole.
	"""
	l=[""]
	i=0
	for p in testo:
		if not p == " ":l[i]+=p
		elif not l[i] == "" and not l[i] == " ":
			l=l+[""]
			i+=1
	return l

"""
ESECUZIONI:

>>> from lezione3_esercizi_sol import getElencoVocaboli2

>>> getElencoVocaboli2(" ciao mamma")
['ciao', 'mamma']
>>> getElencoVocaboli2("ciao   mamma")
['ciao', 'mamma']
>>> getElencoVocaboli2(" ciao   mamma")
['ciao', 'mamma']
>>> getElencoVocaboli2(" ciao   mamma  ")
['ciao', 'mamma', '']
>>> getElencoVocaboli2(" ciao   mamma     ")
['ciao', 'mamma', '']
>>> getElencoVocaboli2("")
['']
>> getElencoVocaboli2(" ")
['']
"""

#Scarta le sottostringhe vuote intermedie ed iniziali, lasciando, tutta via, quella finale. 
def getElencoVocaboli3(testo):
	"""
	Prende in input una stringa: 'testo' contenente parole separate da spazi, e ritorna una lista contenente tali parole.
	"""
	testo+=" "
	l=[]
	s=""
	for p in testo:
		if not p == " ":s+=p
		elif not s == "" and not s == " ":
			l+=[s]
			s=""
	return l
#Scarta ogni occorrenza di sottostringa vuota.

"""
ESECUZIONI:

>>> from lezione3_esercizi_sol import getElencoVocaboli3

>>> getElencoVocaboli3("  ")
[]
>>> getElencoVocaboli3("")
[]
>>> getElencoVocaboli3("ciao mamma")
['ciao', 'mamma']
>>> getElencoVocaboli3("ciao   mamma")
['ciao', 'mamma']
>>> getElencoVocaboli3("ciao   mamma  ")
['ciao', 'mamma']
>>> getElencoVocaboli3(" ciao   mamma  ")
['ciao', 'mamma']
>>> getElencoVocaboli3(" ciao   mamma  miamai")
['ciao', 'mamma', 'miamai']
>>> getElencoVocaboli3(" ciao   mamma  miamai   ")
['ciao', 'mamma', 'miamai']
"""

#Es.2
def rimpiazza(testo, vecchias, nuovas):
	"""
	Prende in input 3 stringhe: testo, vecchias e nuovas, e ritorna una lista contenente:
	- Come primo elemento, una stringa ottenuta da 'testo', sostituendo tutte le occorrenze di 'vecchias' con 'nuovas'.
	- Come secondo elemento, un intero corrispondente al numero di occorrenze che sono state sostituite.
	- Come terzo elemento, l'elenco degl'indici dove sono state trovate le occorrenze (l'indice della prima lettera di ogni occorrenza).
	Se 'vecchias' NON dovesse comparire in testo, ritornerà solo la stringa originale(anche se vuota).
	"""
	idxs=[]
	occ=0
	ln=len(vecchias)
	for i in range(len(testo)):
		if testo[i:i+ln] == vecchias:
			idxs+=[i]
			occ+=1
			tmp=testo[:i]+nuovas+testo[i+ln:]
			testo=tmp
	if not occ: return testo
	return [testo,occ,idxs]

"""
ESECUZIONI:

>>> from lezione3_esercizi_sol import rimpiazza

>>> rimpiazza("","motte","sera")
''
>>> rimpiazza("stanotte sarà una notte bellissima","motte","sera")
'stanotte sarà una notte bellissima'
>>> rimpiazza("stanotte sarà una notte bellissima","notte","sera")
['stasera sarà una sera bellissima', 2, [3, 17]]
>>> rimpiazza("stanotte sarà una notte bellissima notte nottenotte","notte","sera")
['stasera sarà una sera bellissima sera serasera', 5, [3, 17, 33, 38, 42]]
"""

#Es.3
def strENum(s):
	"""
	Prende in input una stringa: 's' contenente parole e numeri(solo interi) separati da spazi, e ritorna una lista contenente:
	- Come primo elemento, la lista delle sole parole(sottostringhe) contenute nella stringa; lista vuota se non presenti.
	- Come secondo elemento, una lista dei soli numeri interi(sottostringhe convertite ad intero) presenti nella stringa; lista vuota se non presenti.
	- Se 's' è vuota, la funzione restituirà una lista vuota (e non una lista di liste vuote).
	"""
	s+=" "
	parole=[]
	numeri=[]
	isnum=False
	t=""
	for p in s:
		if p in ["0","1","2","3","4","5","6","7","8","9"]:
			if not isnum: 
				t=""
				isnum=True
			t+=p
		elif not p == " ":
			if isnum: 
				#t="" #questa esclusione consente di accettare sottostringhe che iniziano con una seq. numerica, ma non che ne finiscono.
				isnum=False
			t+=p
		elif not t == "" and not t == " " and isnum:
			numeri+=[int(t)]
			t=""
		elif not t == "" and not t == " " and not isnum:
			parole+=[t]
			t=""
	if not parole and not numeri: return []
	return [parole,numeri]

"""
ESECUZIONI:

>>> from lezione3_esercizi_sol import strENum

>>> strENum("123ciao234 123 ")
[[], [234, 123]]
>>> strENum("123ciao 123 ")
[['123ciao'], [123]]
>>> strENum("123ciao234 123 ")
[[], [234, 123]]
>>> strENum("123ciao 123 ")
[['123ciao'], [123]]
>>> strENum("ciao 123 ")
[['ciao'], [123]]
>>> strENum("ciao 123 6")
[['ciao'], [123, 6]]
>>> strENum("ciao 123 6 99")
[['ciao'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 k8i")
[['ciao', '8i'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 ki")
[['ciao', 'ki'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 k8")
[['ciao'], [123, 6, 99, 8]]
>>> strENum("ciao 123 6 99 k8 msca")
[['ciao', 'msca'], [123, 6, 99, 8]]
>>> strENum("  ciao  123 6   99 k8 msca   ")
[['ciao', 'msca'], [123, 6, 99, 8]]
"""

#Es.4
def alteraLista(dest, sorg, da, a):
	"""
	Prende in input 2 liste: 'dest', 'sorg' e due interi 'DA', 'A', e ritorna una NUOVA lista ottenuta da 'dest', sostituendo gli elementi dall'indice 'DA', all'indice 'A', con tutti quelli presenti in 'sorg'.
	Gli elementi nell'intervallo 'DA'-'A' saranno eliminati ed al loro posto ci saranno tutti gli elementi contenuti in 'sorg'.
	"""
	return dest[:da]+sorg+dest[a:]

"""
ESECUZIONI:

>>> from lezione3_esercizi_sol import alteraLista

>>> alteraLista([1,2,3],["Bianco",True],1,2)
[1, 'Bianco', True, 3]
>>> int in [1,2,"ciao"]
False
>>> int in [1,2,"ciao",int]
True
"""

#Es.5
def filtraERaggruppa(tipi, ogg):
	"""
	Prende in input 2 liste: 
	- 'tipi': contenente un elenco di tipi (int, float, str, list, ecc..).
	- 'ogg': contenente oggetti di varie tipologie.
	Ritorna una NUOVA lista contenente gli oggetti in 'ogg', ordinati per tipologia, secondo l'ordine in cui, tali tipologie, compaiono in 'tipi'.
	Se in 'ogg' sono presenti oggetti di una tipologia non elencata in 'tipi', quest'ultimi NON compariranno nella lista risultante.
	"""
	l=[]
	for t in tipi:
		for o in ogg:
			if type(o) == t: l+=[o]
	return l
		
"""
ESECUZIONI:

>>> from lezione3_esercizi_sol import filtraERaggruppa

>>> filtraERaggruppa([str,int,list,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
['ciao', 1, 2, 4, 5, 18, [1, 2], True, False]
>>> filtraERaggruppa([int,list,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, [1, 2], True, False]
>>> filtraERaggruppa([int,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, True, False]
>>> filtraERaggruppa([int,bool,int],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, True, False, 1, 2, 4, 5, 18]
>>> filtraERaggruppa([bool,int],[1,2,"ciao",4,True,5,False,18,[1,2]])
[True, False, 1, 2, 4, 5, 18]
"""
