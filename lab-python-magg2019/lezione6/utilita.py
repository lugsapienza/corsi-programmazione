"""
ESERCITAZIONE SUI MODULI

Scrivere un modulo 'utilita' che possa essere utilizzato nelle seguenti due modalità:
1) script: Dovrà eseguire la print di una stringa che descriva le funzionalità del modulo.
		   Esempio:
            print(\"\"\"Questo modulo definisce i simboli:
				shiftPosizione(arg, i)
				shiftPosizioneInversa(arg, i)
			Utilizzo: import utilita.py
			\"\"\")
			
2) import: Dovrà definire i seguenti simboli:
""" 

def shiftPosizione(arg, i):
	"""
	Questa funzione prende in input una stringa arg (effettuate un contollo 
	che sia tale) ed un intero i e sposti SINISTRA di i posizioni i caratteri contenuti
	in arg ritornando la nuova stringa.
	
	Es:
	arg = "abc", i = 1
	return = "bca"
	
	arg = "abcde", i = 2
	return = "cdeab"
	
	"""
	pass


def shiftPosizioneInversa(arg, i):
	"""
	Questa funzione prende in input una stringa arg (effettuate un contollo 
	che sia tale) ed un intero i e sposti DESTRA di i posizioni i caratteri contenuti
	in arg ritornando la nuova stringa.
	
	Es:
	arg = "abc", i = 1
	return = "cab"
	
	arg = "abcde", i = 2
	return = "deabc"
	"""
	pass
