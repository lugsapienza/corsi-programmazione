if __name__ == "__main__":
	print("""Questo modulo implementa le seguenti funzioni:
			- shiftPosizione(arg,i)
			- shiftPosizioneInversa(arg,i)
		""")
else:
	def shiftPosizione(arg, i):
		"""
		Questa funzione prende in input una stringa arg (effettuate un contollo 
		che sia tale) ed un intero i e sposti SINISTRA di i posizioni i caratteri contenuti
		in arg ritornando la nuova stringa.	
		
		Es:
		arg = "abc", i = 1
		return = "bca"
		
		arg = "abcde", i = 2
		return = "cdeab"
		"""
		
		if type(arg) != str or type(i) != int: return
		ret = arg[i:] + arg[0:i]
		return ret


	def shiftPosizioneInversa(arg, i):
		"""
		Questa funzione prende in input una stringa arg (effettuate un contollo
		che sia tale) ed un intero i e sposti DESTRA di i posizioni i caratteri contenuti
		in arg ritornando la nuova stringa.
		
		Es:
		arg = "abc", i = 1
		return = "cab"
		
		arg = "abcde", i = 2
		return = "deabc"
		"""
		
		if type(arg) != str or type(i) != int: return
		ret = arg[-i:] + arg[0:-i]
		return ret
