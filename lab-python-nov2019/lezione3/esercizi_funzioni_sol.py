"""
Questo script contiene firma e DocString di funzioni che dovranno essere implementate.
Sostituire la parola 'pass', con l'implementazione della funzione.

Per poter provare la correttezza delle varie implementazioni, è possibile IMPORTARE i simboli definiti in questo file,
all'interno della shell interattiva, in modo tale da poterne chiamare le varie funzioni, 
come se fossero state definite all'interno della shell.

Per fare ciò, utilizzando la shell interattiva dell'IDLE di Python, è sufficiente eseguire tale script(Run Module),
e, nella shell che si aprirà, mostrando eventuali output visivi(nessuno in tal caso), chiamare le funzioni desiderate.


---- ESEMPIO (RUN MODULE)----
Python 3.7.3 (v3.7.3:ef4ec6ed12, Mar 25 2019, 16:52:21) 
[Clang 6.0 (clang-600.0.57)] on darwin
Type "help", "copyright", "credits" or "license()" for more information.
>>> 
===================== RESTART: /../../esercizi_funzioni.py =====================

>>> getElencoVocaboli("ciao mamma")   #FUNZIONE DEFINITA DENTRO: esercizi_funzioni.py
['ciao', 'mamma']

-----------------------------
E' anche possibile importare tale modulo nella shell interattiva eseguita su interfaccia testuale(CMD o Terminale), in questo modo: 

$cd [CARTELLA DOVE SI TROVA IL FILE: esercizi_funzioni.py]
$python3
Python 3.7.3 (v3.7.3:ef4ec6ed12, Mar 25 2019, 16:52:21) 
[Clang 6.0 (clang-600.0.57)] on darwin
Type "help", "copyright", "credits" or "license()" for more information.

>>> from esercizi_funzioni import *     #IMPORTAZIONE DEI SIMBOLI DEFINITI IN esercizi_funzioni.py
>>> getElencoVocaboli("ciao mamma")
['ciao', 'mamma']


In caso di modifiche apportate alle funzioni qui definite, tale modulo dovrà essere ri-eseguito, 
prima di chiamarne nuovamente le funzioni che sono state modificate(per poterne vedere i cambiamenti).
"""



#----------ESERCIZIO 1:
def getElencoVocaboli(testo):
	"""
	Prende in input una stringa: 'testo' contenente parole separate da spazi, e ritorna una lista contenente tali parole.
	"""
	l=[""]
	i=0
	for p in testo:
		if not p == " ":l[i]+=p
		else:
			l=l+[""]
			i+=1
	return l

#Non scarta le sottostringhe vuote in alcun modo 
def getElencoVocaboli2(testo):
	"""
	Prende in input una stringa: 'testo' contenente parole separate da spazi, e ritorna una lista contenente tali parole.
	"""
	l=[""]
	i=0
	for p in testo:
		if not p == " ":l[i]+=p
		elif not l[i] == "" and not l[i] == " ":
			l=l+[""]
			i+=1
	return l

"""
ESEMPI D'ESECUZIONE:

>>> getElencoVocaboli2(" ciao mamma")
['ciao', 'mamma']
>>> getElencoVocaboli2("ciao   mamma")
['ciao', 'mamma']
>>> getElencoVocaboli2(" ciao   mamma")
['ciao', 'mamma']
>>> getElencoVocaboli2(" ciao   mamma  ")
['ciao', 'mamma', '']
>>> getElencoVocaboli2(" ciao   mamma     ")
['ciao', 'mamma', '']
>>> getElencoVocaboli2("")
['']
>> getElencoVocaboli2(" ")
['']
"""

#Scarta le sottostringhe vuote intermedie ed iniziali, lasciando, tutta via, quella finale. 
def getElencoVocaboli3(testo):
	"""
	Prende in input una stringa: 'testo' contenente parole separate da spazi, e ritorna una lista contenente tali parole.
	"""
	testo+=" "
	l=[]
	s=""
	for p in testo:
		if not p == " ":s+=p
		elif not s == "" and not s == " ":
			l+=[s]
			s=""
	return l
#Scarta ogni occorrenza di sottostringa vuota.

"""
ESEMPI D'ESECUZIONE:

>>> getElencoVocaboli3("  ")
[]
>>> getElencoVocaboli3("")
[]
>>> getElencoVocaboli3("ciao mamma")
['ciao', 'mamma']
>>> getElencoVocaboli3("ciao   mamma")
['ciao', 'mamma']
>>> getElencoVocaboli3("ciao   mamma  ")
['ciao', 'mamma']
>>> getElencoVocaboli3(" ciao   mamma  ")
['ciao', 'mamma']
>>> getElencoVocaboli3(" ciao   mamma  miamai")
['ciao', 'mamma', 'miamai']
>>> getElencoVocaboli3(" ciao   mamma  miamai   ")
['ciao', 'mamma', 'miamai']
"""

#----------ESERCIZIO 2:
def rimpiazza(testo, vecchias, nuovas):
	"""
	Prende in input 3 stringhe: testo, vecchias e nuovas, e ritorna una lista contenente:
	- Come primo elemento, una stringa ottenuta da 'testo', sostituendo tutte le occorrenze di 'vecchias' con 'nuovas'.
	- Come secondo elemento, un intero corrispondente al numero di occorrenze che sono state sostituite.
	- Come terzo elemento, l'elenco degl'indici dove sono state trovate le occorrenze (l'indice della prima lettera di ogni occorrenza).
	Se 'vecchias' NON dovesse comparire in testo, ritornerà solo la stringa originale(anche se vuota).
	"""
	idxs=[]
	occ=0
	ln=len(vecchias)
	for i in range(len(testo)):
		if testo[i:i+ln] == vecchias:
			idxs+=[i]
			occ+=1
			tmp=testo[:i]+nuovas+testo[i+ln:]
			testo=tmp
	if not occ: return testo
	return [testo,occ,idxs]

"""
ESEMPI D'ESECUZIONE:

>>> rimpiazza("","motte","sera")
''
>>> rimpiazza("stanotte sarà una notte bellissima","motte","sera")
'stanotte sarà una notte bellissima'
>>> rimpiazza("stanotte sarà una notte bellissima","notte","sera")
['stasera sarà una sera bellissima', 2, [3, 17]]
>>> rimpiazza("stanotte sarà una notte bellissima notte nottenotte","notte","sera")
['stasera sarà una sera bellissima sera serasera', 5, [3, 17, 33, 38, 42]]
"""

#----------ESERCIZIO 3:
def strENum(s):
	"""
	Prende in input una stringa: 's' contenente parole e numeri(solo interi) separati da spazi, e ritorna una lista contenente:
	- Come primo elemento, la lista delle sole parole(sottostringhe) contenute nella stringa; lista vuota se non presenti.
	- Come secondo elemento, una lista dei soli numeri interi(sottostringhe convertite ad intero) presenti nella stringa; lista vuota se non presenti.
	- Se 's' è vuota, la funzione restituirà una lista vuota (e non una lista di liste vuote).
	"""
	s+=" "
	parole=[]
	numeri=[]
	isnum=False
	t=""
	for p in s:
		if p in ["0","1","2","3","4","5","6","7","8","9"]:
			if not isnum: 
				t=""
				isnum=True
			t+=p
		elif not p == " ":
			if isnum: 
				#t="" #questa esclusione consente di accettare sottostringhe che iniziano con una seq. numerica, ma non che ne finiscono.
				isnum=False
			t+=p
		elif not t == "" and not t == " " and isnum:
			numeri+=[int(t)]
			t=""
		elif not t == "" and not t == " " and not isnum:
			parole+=[t]
			t=""
	if not parole and not numeri: return []
	return [parole,numeri]

"""
ESEMPI D'ESECUZIONE:

>>> strENum("123ciao234 123 ")
[[], [234, 123]]
>>> strENum("123ciao 123 ")
[['123ciao'], [123]]
>>> strENum("123ciao234 123 ")
[[], [234, 123]]
>>> strENum("123ciao 123 ")
[['123ciao'], [123]]
>>> strENum("ciao 123 ")
[['ciao'], [123]]
>>> strENum("ciao 123 6")
[['ciao'], [123, 6]]
>>> strENum("ciao 123 6 99")
[['ciao'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 k8i")
[['ciao', '8i'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 ki")
[['ciao', 'ki'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 k8")
[['ciao'], [123, 6, 99, 8]]
>>> strENum("ciao 123 6 99 k8 msca")
[['ciao', 'msca'], [123, 6, 99, 8]]
>>> strENum("  ciao  123 6   99 k8 msca   ")
[['ciao', 'msca'], [123, 6, 99, 8]]
"""

#----------ESERCIZIO 4:
def alteraLista(dest, sorg, da, a):
	"""
	Prende in input 2 liste: 'dest', 'sorg' e due interi 'DA', 'A', e ritorna una NUOVA lista ottenuta da 'dest', sostituendo gli elementi dall'indice 'DA', all'indice 'A', con tutti quelli presenti in 'sorg'.
	Gli elementi nell'intervallo 'DA'-'A' saranno eliminati ed al loro posto ci saranno tutti gli elementi contenuti in 'sorg'.
	"""
	return dest[:da]+sorg+dest[a:]

"""
ESEMPI D'ESECUZIONE:

>>> alteraLista([1,2,3],["Bianco",True],1,2)
[1, 'Bianco', True, 3]
>>> int in [1,2,"ciao"]
False
>>> int in [1,2,"ciao",int]
True
"""

#----------ESERCIZIO 5:
def filtraERaggruppa(tipi, ogg):
	"""
	Prende in input 2 liste: 
	- 'tipi': contenente un elenco di tipi (int, float, str, list, ecc..).
	- 'ogg': contenente oggetti di varie tipologie.
	Ritorna una NUOVA lista contenente gli oggetti in 'ogg', ordinati per tipologia, secondo l'ordine in cui, tali tipologie, compaiono in 'tipi'.
	Se in 'ogg' sono presenti oggetti di una tipologia non elencata in 'tipi', quest'ultimi NON compariranno nella lista risultante.
	"""
	l=[]
	for t in tipi:
		for o in ogg:
			if type(o) == t: l+=[o]
	return l
		
"""
ESEMPI D'ESECUZIONE:

>>> filtraERaggruppa([str,int,list,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
['ciao', 1, 2, 4, 5, 18, [1, 2], True, False]
>>> filtraERaggruppa([int,list,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, [1, 2], True, False]
>>> filtraERaggruppa([int,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, True, False]
>>> filtraERaggruppa([int,bool,int],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, True, False, 1, 2, 4, 5, 18]
>>> filtraERaggruppa([bool,int],[1,2,"ciao",4,True,5,False,18,[1,2]])
[True, False, 1, 2, 4, 5, 18]
"""


#----------ESERCIZIO 6:
def massimo(l):
	"""
	Prende in input una lista: l, NON vuota, di numeri Reali, e ne ritorna in output quello più grande.
	In caso di lista vuota, o di un oggetto di tipo differente, la funzione ritorna il valore nullo: None.
	In caso di oggetto differente da un numero intero o reale nella lista, la funzione ritorna il valore nullo: None.
	"""
	if not type(l) == list or not l: return None
	max=l[0]
	for e in l:
		if not type(e) == int and not type(e) == float: return None
		if e > max: max=e
	return max

"""
ESEMPI D'ESECUZIONE:

>>> massimo([1, 1.0, 2.3])
2.3
"""


#----------ESERCIZIO 7:
def numero_palindromo(n):
	"""
	Dato un numero intero n in input, ritorna True se palindromo, False altrimenti.
	In caso di oggetto differente da un numero intero, la funzione ritorna il valore nullo: None.
	"""
	if not type(n) == int: return None
	num=n
	pal=0
	while num:
		pal = pal * 10 + num%10
		num //= 10
	if pal == n: return True
	return False

"""
ESEMPI D'ESECUZIONE:

>>> numero_palindromo(1221)
True
>>> numero_palindromo(1223)
False
"""


#----------ESERCIZIO 8:
def genera_istogramma(l, simbolo="*"):
	"""
	Prende in input una lista di interi, ed un simbolo(stringa di 1 carattere) e restituisce una nuova stringa 
	contenente l'istogramma dei valori presenti in lista, disegnato utilizzando il simbolo specificato in input.
	
	'simbolo' è una stringa, di lunghezza 1, utilizzata per disegnare l'istogramma.
	Di default il suo valore è: "*".
	
	In caso di oggetto NON lista, lista vuota o elemento NON numerico intero presente in lista, 
	la funzione ritorna il valore nullo: None.
	"""
	if not type(l) == list or not l or not type(simbolo) == str or not len(simbolo) == 1: return None
	llen=len(l)
	ig=""
	for val in range(llen):
		if not type(l[val]) == int: return None
		ig += simbolo*l[val]
		if val < llen-1: ig+="\n\n"
	return ig

"""
ESEMPI D'ESECUZIONE:

>>> genera_istogramma([1,2,2,4], "*")
*
**
**
****
>>> genera_istogramma([3,2,5], "-")
---
--
-----

"""
