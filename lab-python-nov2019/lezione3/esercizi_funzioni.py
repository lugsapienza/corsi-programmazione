"""
Questo script contiene firma e DocString di funzioni che dovranno essere implementate.
Sostituire la parola 'pass', con l'implementazione della funzione.

Per poter provare la correttezza delle varie implementazioni, è possibile IMPORTARE i simboli definiti in questo file,
all'interno della shell interattiva, in modo tale da poterne chiamare le varie funzioni, 
come se fossero state definite all'interno della shell.

Per fare ciò, utilizzando la shell interattiva dell'IDLE di Python, è sufficiente eseguire tale script(Run Module),
e, nella shell che si aprirà, mostrando eventuali output visivi(nessuno in tal caso), chiamare le funzioni desiderate.


---- ESEMPIO (RUN MODULE)----
Python 3.7.3 (v3.7.3:ef4ec6ed12, Mar 25 2019, 16:52:21) 
[Clang 6.0 (clang-600.0.57)] on darwin
Type "help", "copyright", "credits" or "license()" for more information.
>>> 
===================== RESTART: /../../esercizi_funzioni.py =====================

>>> getElencoVocaboli("ciao mamma")   #FUNZIONE DEFINITA DENTRO: esercizi_funzioni.py
['ciao', 'mamma']

-----------------------------
E' anche possibile importare tale modulo nella shell interattiva eseguita su interfaccia testuale(CMD o Terminale), in questo modo: 

$cd [CARTELLA DOVE SI TROVA IL FILE: esercizi_funzioni.py]
$python3
Python 3.7.3 (v3.7.3:ef4ec6ed12, Mar 25 2019, 16:52:21) 
[Clang 6.0 (clang-600.0.57)] on darwin
Type "help", "copyright", "credits" or "license()" for more information.

>>> from esercizi_funzioni import *     #IMPORTAZIONE DEI SIMBOLI DEFINITI IN esercizi_funzioni.py
>>> getElencoVocaboli("ciao mamma")
['ciao', 'mamma']


In caso di modifiche apportate alle funzioni qui definite, tale modulo dovrà essere ri-eseguito, 
prima di chiamarne nuovamente le funzioni che sono state modificate(per poterne vedere i cambiamenti).
"""



#----------ESERCIZIO 1:
def getElencoVocaboli(testo):
	"""
	Prende in input una stringa: 'testo' contenente parole separate da spazi, e ritorna una lista contenente tali parole.
	"""
	pass

"""
ESEMPI D'ESECUZIONE:

>>> getElencoVocaboli("  ")
[]
>>> getElencoVocaboli("")
[]
>>> getElencoVocaboli("ciao mamma")
['ciao', 'mamma']
>>> getElencoVocaboli("ciao   mamma")
['ciao', 'mamma']
>>> getElencoVocaboli("ciao   mamma  ")
['ciao', 'mamma']
>>> getElencoVocaboli(" ciao   mamma  ")
['ciao', 'mamma']
>>> getElencoVocaboli(" ciao   mamma  miamai")
['ciao', 'mamma', 'miamai']
>>> getElencoVocaboli(" ciao   mamma  miamai   ")
['ciao', 'mamma', 'miamai']
"""


#----------ESERCIZIO 2:
def rimpiazza(testo, vecchias, nuovas):
	"""
	Prende in input 3 stringhe: testo, vecchias e nuovas, e ritorna una lista contenente:
	- Come primo elemento, una stringa ottenuta da 'testo', sostituendo tutte le occorrenze di 'vecchias' con 'nuovas'.
	- Come secondo elemento, un intero corrispondente al numero di occorrenze che sono state sostituite.
	- Come terzo elemento, l'elenco degl'indici dove sono state trovate le occorrenze (l'indice della prima lettera di ogni occorrenza).
	Se 'vecchias' NON dovesse comparire in testo, ritornerà solo la stringa originale(anche se vuota).
	"""
	pass

"""
ESEMPI D'ESECUZIONE:

>>> rimpiazza("","motte","sera")
''
>>> rimpiazza("stanotte sarà una notte bellissima","motte","sera")
'stanotte sarà una notte bellissima'
>>> rimpiazza("stanotte sarà una notte bellissima","notte","sera")
['stasera sarà una sera bellissima', 2, [3, 17]]
>>> rimpiazza("stanotte sarà una notte bellissima notte nottenotte","notte","sera")
['stasera sarà una sera bellissima sera serasera', 5, [3, 17, 33, 38, 42]]
"""


#----------ESERCIZIO 3:
def strENum(s):
	"""
	Prende in input una stringa: 's' contenente parole e numeri(solo interi) separati da spazi, e ritorna una lista contenente:
	- Come primo elemento, la lista delle sole parole(sottostringhe) contenute nella stringa; lista vuota se non presenti.
	- Come secondo elemento, una lista dei soli numeri interi(sottostringhe convertite ad intero) presenti nella stringa; lista vuota se non presenti.
	- Se 's' è vuota, la funzione restituirà una lista vuota (e non una lista di liste vuote).
	"""
	pass

"""
ESEMPI D'ESECUZIONE:

>>> strENum("123ciao234 123 ")
[[], [234, 123]]
>>> strENum("123ciao 123 ")
[['123ciao'], [123]]
>>> strENum("123ciao234 123 ")
[[], [234, 123]]
>>> strENum("123ciao 123 ")
[['123ciao'], [123]]
>>> strENum("ciao 123 ")
[['ciao'], [123]]
>>> strENum("ciao 123 6")
[['ciao'], [123, 6]]
>>> strENum("ciao 123 6 99")
[['ciao'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 k8i")
[['ciao', '8i'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 ki")
[['ciao', 'ki'], [123, 6, 99]]
>>> strENum("ciao 123 6 99 k8")
[['ciao'], [123, 6, 99, 8]]
>>> strENum("ciao 123 6 99 k8 msca")
[['ciao', 'msca'], [123, 6, 99, 8]]
>>> strENum("  ciao  123 6   99 k8 msca   ")
[['ciao', 'msca'], [123, 6, 99, 8]]
"""


#----------ESERCIZIO 4:
def alteraLista(dest, sorg, da, a):
	"""
	Prende in input 2 liste: 'dest', 'sorg' e due interi 'DA', 'A', e ritorna una NUOVA lista ottenuta da 'dest', sostituendo gli elementi dall'indice 'DA', all'indice 'A', con tutti quelli presenti in 'sorg'.
	Gli elementi nell'intervallo 'DA'-'A' saranno eliminati ed al loro posto ci saranno tutti gli elementi contenuti in 'sorg'.
	"""
	pass

"""
ESEMPI D'ESECUZIONE:

>>> alteraLista([1,2,3],["Bianco",True],1,2)
[1, 'Bianco', True, 3]
>>> int in [1,2,"ciao"]
False
>>> int in [1,2,"ciao",int]
True
"""


#----------ESERCIZIO 5:
def filtraERaggruppa(tipi, ogg):
	"""
	Prende in input 2 liste: 
	- 'tipi': contenente un elenco di tipi (int, float, str, list, ecc..).
	- 'ogg': contenente oggetti di varie tipologie.
	Ritorna una NUOVA lista contenente gli oggetti in 'ogg', ordinati per tipologia, secondo l'ordine in cui, tali tipologie, compaiono in 'tipi'.
	Se in 'ogg' sono presenti oggetti di una tipologia non elencata in 'tipi', quest'ultimi NON compariranno nella lista risultante.
	"""
	pass
	
"""
ESEMPI D'ESECUZIONE:

>>> filtraERaggruppa([str,int,list,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
['ciao', 1, 2, 4, 5, 18, [1, 2], True, False]
>>> filtraERaggruppa([int,list,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, [1, 2], True, False]
>>> filtraERaggruppa([int,bool],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, True, False]
>>> filtraERaggruppa([int,bool,int],[1,2,"ciao",4,True,5,False,18,[1,2]])
[1, 2, 4, 5, 18, True, False, 1, 2, 4, 5, 18]
>>> filtraERaggruppa([bool,int],[1,2,"ciao",4,True,5,False,18,[1,2]])
[True, False, 1, 2, 4, 5, 18]
"""


#----------ESERCIZIO 6:
"""
'Massimo, il reale'

Implementare la funzione: massimo(l), che data in input una lista: l, NON vuota, di numeri Reali, ne ritorni in output quello più grande.
In caso di lista vuota, o di un oggetto di tipo differente, la funzione dovrà ritornare il valore nullo: None.
Nel caso in cui la lista dovesse contenere un oggetto differente da un numero reale, la funzione dovrà ritornare il valore nullo: None.
"""
def massimo(l):pass

"""
ESEMPI D'ESECUZIONE:

>>> massimo([1, 1.0, 2.3])
2.3
"""


#----------ESERCIZIO 7:
"""
'Il Palindromo'

Un numero è palindromo quando le sue cifre rappresentano lo stesso valore, sia se lette partendo da destra, che da sinistra.

Implementare la funzione: numero_palindromo(n), che dato in input un numero intero, ritorni True se tale numero è palindromo, False altrimenti.
Nel caso in cui l'oggetto passato non dovesse essere un intero, la funzione dovrà ritornare il valore nullo: None.
"""
def numero_palindromo(n):pass

"""
ESEMPI D'ESECUZIONE:

>>> numero_palindromo(1221)
True
>>> numero_palindromo(1223)
False
"""


#----------ESERCIZIO 8:
"""
'Istogram'

Implementare la funzione: genera_istogramma(l, simbolo), che data in input una lista di interi ed un simbolo(stringa di 1 carattere),
restituisca una nuova stringa contenente l'istogramma dei valori presenti in lista, disegnato utilizzando il simbolo specificato in input.

La funzione deve poter essere chiamata senza specificare il simbolo da utilizzare nell'istogramma; in tal caso la funzione adotterà il
simbolo di default: '*'
Nel caso in cui l'oggetto passato non dovesse essere una lista, la lista dovesse essere nulla, o dovesse contenere oggetti non di tipo intero,
la funzione dovrà ritornare il valore nullo: None.
"""
def genera_istogramma(l, simbolo):pass

"""
ESEMPI D'ESECUZIONE:

>>> genera_istogramma([1,2,2,4], "*")
*
**
**
****
>>> genera_istogramma([3,2,5], "-")
---
--
-----

"""