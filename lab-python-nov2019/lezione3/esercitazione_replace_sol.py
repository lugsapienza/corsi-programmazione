#ESERCITAZIONE:
""" Scrivere una funzione: replace(source(str), string(str), newstring(str)) -> str 
che prende tre stringhe in input e ritorna una stringa.
Tale funzione dovrà rimpiazzare tutte le occorrenze di   string  in source, con newstring,
ritornando la nuova stringa ottenuta.

In caso di nessuna occorrenza di  string  in  source, la funzione ritornerà  source.
"""

#SOLUZIONE:
def replace(source, string, newstring):
	"""
	source: str, string: str, newstring: str.
	Rimpiazza tutte le occorrenze di string, in source, con newstring.
	Ritorna la nuova stringa generata.
	"""
	if not type(source) == str or not type(string) == str or not type(newstring) == str:
		return
	
	for i in range(len(source)):
		if source[i:i+len(string)] == string: 
			source=source[:i]+newstring+source[i+len(string):]
	return source
