doc = """
Questo modulo definisce le funzioni:
- len(string): int 
    Prende in input una stringa e ne ritorna la lunghezza in output.
- reverse(string): string
    Prende in input una stringa e ne ritorna la stringa invertita in output.

Utilizzo: 
import strutil
strutil.[simbolo_richiesto]

oppure

from strutil import [simbolo_richiesto], [simbolo_richiesto], ...
[simbolo_richiesto]
"""

if __name__ == "__main__": print(doc) #Stampa la modalità di utilizzo, in caso di esecuzione come script.
else:  #Altrimenti definisci i simboli illustrati.
    def len(s):
        if not type(s) == str: return
        #print("MIA LEN") #de-commentare per fare il confronto con la 'len' predefinita.
        count=0
        for c in s: count+=1
        return count

    def reverse(s):
        if not type(s) == str: return
        newstr = ""
        for c in s :
            newstr = c + newstr
        return newstr
