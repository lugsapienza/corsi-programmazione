"""
A differenza di altri linguaggi, in Python, i simboli definiti all'interno di un blocco condizionale o ciclico
rimangono visibili all'esterno di tali blocchi, perché memorizzati nella tabella dei simboli globale.
Al contrario, simboli definiti in funzioni o file differenti(script/moduli), non sono direttamente visibile all'esterno.
"""
#Esempio:
if True:
    a = 0
print("a =",a) #'a' è visibile, nonostante sia stata definita all'interno dell'if.


"""
È sconsigliato l’utilizzo di global e nonlocal, salvo in casi come quello citato al punto precedente, o per stretta necessità.
Il loro utilizzo crea forte accoppiamento(legame stretto) tra il codice di una funzione, e quello definito esternamente, 
andando contro i principi di modularità che una funzione dovrebbe rispettare, e rendendone, il codice, poco riutilizzabile.

Nel seguente esempio viene mostrata un'implementazione della funzione: len, che invece di prendere la stringa: s come parametro,
referenzia un simbolo: s, definito esternamente.
Tale implementazione crea alto accomppiamento tra il codice della funzione e quello definito esternamente, 
perché quest'ultimo deve definire un simbolo: s, associto ad una stringa, per far si che 'len' possa funzionare.
Tutto ciò rende poco modulare e riutilizzabile tale implementazione della funzione.
"""
#Esempio:
s="ciao"
def len(): #len(s):
    global s   #'s' dev'essere definito esternamente, perché questo codice possa funzionare.
    count=0
    for c in s: count+=1
    return count
print("Output funzione 'len':",len())#len("ciao"))


"""
Per utilizzare i simboli definiti all'interno di un modulo(file differente da quello corrente, contenente SOLO definizioni di simboli),
è necessario importare il simbolo corrispondente al suo nome, nella tabella dei simboli del file corrente:
import nomeFile(senza l'estensione)

Fatto ciò è possibile utilizzarne i simboli in questo modo: modulo.simbolo
"""
#Esempio:
import strutil
print(strutil.len("ciao"))
print(strutil.reverse("ciao"))


del strutil #Rimuove il simbolo 'strutil' dalla tabella corrente(globale).
#E' anche possibile importare solo i simboli richiesti, direttamente nella tabella del modulo corrente:
from strutil import len
print(len("ciao"))
print(strutil.len("ciao"))  #'strutil' non è stato importato, ma solo il simbolo: 'len' in esso definito.


"""
Essendo stato importato, dal modulo: strutil, il simbolo: len, definito anche nel modulo standard: builtins,
tutte le future chiamate alla len, utilizzeranno l'implementazione definita in strutil, e non quella predefinita.

Per poter utilizzare nuovamente l'implementazione predefinita, è sufficiente specificare il modulo contenente l'implementazione
che si vuole utilizzare(builtins).
"""
#Esempio:
print(len("ciao"))
print(__builtins__.len("ciao"))


"""
Così come è possibile ridefinire simboli locali, è anche possibile ridefinire simboli esterni(se sono raggiungibili, si possono riassegnare),
mediante l'operatore di assegnazione.
"""
#Esempio:
__builtins__.len = len #Questa modifica sarà locale alla corrente esecuzione di questo file(nuove esecuzioni adotteranno il valore predefinito).
print(__builtins__.len("ciao"))


"""
Quando l'interprete incontra l'import di un modulo, per la prima volta, eseguirà il suo codice,
con lo scopo di definire tutti i simboli in esso definiti.
Eventuali import successive, dello stesso modulo, non comporteranno ulteriori esecuzioni(i suoi simboli sono già stati definiti).
"""
#Esempio(per eseguirlo correttamente, commentare tutte le precedenti righe, contenenti il simbolo: strutil):
print("Esecuzione prima import:")
import strutil
print("Esecuzione seconda import:")
import strutil