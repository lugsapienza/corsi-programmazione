"""
Calcolatrice Base

Creare una calcolatrice che sia in grado di valutare stringhe fornite in input(associate al simbolo: input), 
contenenti espressioni matematiche nella forma: [operando1][operatore][operando2], e stamparne a schermo il risultato.
operando1 ed operando2 sono numeri interi o reali, positivi o negativi.
L'operatore equivale ad una delle seguenti operazioni matematiche: + (somma), - (sottrazione), * (moltiplicazione), / (divisione).

Sviluppare il programma utilizzando i moduli (preferibilmente almeno 2) 
e tenendo conto dei possibili errori dovuti alla specifica di un valore numerico errato(es: “ciao”) 
o al passaggio di un'espressione matematica non valida(es: ++23, 2-u, 23--5, ciao/mamma, ecc..).

Consigli:
Implementare un modulo di supporto (es: util.py) che definisca le funzioni da utilizzare nel modulo “main”.
Es: 
	•	Funzioni per le operazioni: +, -, *, /
	•	Funzioni per la validazione della stringa di input.
    •   Funzioni per l’estrazione delle componenti: operando1, operatore, operando2, dalla stringa di input.

Sta al programmatore la decisione di suddividere il programma in N funzioni o moduli, 
ognuno con una specifica funzionalità da eseguire, che possano renderne più facile lo sviluppo e più leggibile e modulare il codice.

Implementarla prima con i soli numeri interi, senza segno e senza tenere conto delle regole di precedenza degli operatori.
Successivamente:
	•	Aggiungere il supporto per i numeri reali(es: '1.2+2.3' => 3.5).
	•	Aggiungere il supporto per i numeri con segno(es: '+1-2*4' => -4).
	•	Aggiungere il supporto per le regole di precedenza degli operatori(es: '+1-2*4' => -7).
"""



input = "2+3*4"

#-----------------
#-->SCRIVI SOLUZIONE QUI<--
#-----------------

