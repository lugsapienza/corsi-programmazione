#Così come è possibile definire simboli LOCALI ad una funzione; essendo anche le funzioni
#associate ad un simbolo(il loro nome), è possibile definirle LOCALI AD ALTRE FUNZIONI,
#crendo quelle che vengono chiamate FUNZIONI ANNIDATE:

def f():
    print("Im f")
    def f2(): #f2 è locale ad(annidata in) f, cioè visibile solo ad essa.
        print("Im f2, and i know it")
    return f2#return f2()  #cosa cambia tra le due?

(f())() #Chiamata alla funzione restituita come risultato dalla chiamata alla funzione: f
#oppure:
f()()  #Chiamata alla funzione restituita come risultato dalla chiamata alla funzione: f
#Nel caso in cui f non ritorni l'indirizzo di f2, ma il risultato di quest'ultima(chiamandola),
#queste due chiamate generaranno un errore.

r=f() #Chiamata ad f, con salvataggio del risultato in r.
r() #Essendo, il risultato di f, l'indirizzo di una funzione, posso aggiungere le parentesi tonde per eseguire tale funzione.
#oppure:
r=f
r()()

#E' anche possibile definire delle funzioni avanti parametri di input FACOLTATIVI.
#Ciò vuol dire che, se non venissero passati alla sua chiamata, l'inteprete gli assegnerebbe
#i valori di default specificati, tramite l'assegnazione, durante la definizione, in questo modo:

def somma(n1=0,n2=0): #n1 ed n2 sono facoltativi
    return n1+n2

print("Ris: ",somma()) #se non specificati alla chiamata, assumeranno i valori predefiniti.
print("Ris: ",somma(2,3)) #se specificati, varranno utilizzati i nuovi valori.

#Nelle precedenti chiamate i parametri sono stati passati in modo posizionale
#(il primo valore al primo parametro, il secondo al secondo, e così via).
#E' anche possibile dichiarare esplicitamente a quale parametro si vuole assegnare un certo valore
#specificando il nome del parametro(simbolo), l'operatore di assegnazione, ed il valore che gli si vuole assegnare:
print("Ris: ",somma(n2=7)) #si vuole assegnare un nuovo valore solo al parametro n2; n1 manterrà quello predefinito.


#La funzione dir(), se chiamata senza parametri, restituisce una lista contenente tutti i simboli presenti nella tabella globale.
""" 
>>> dir()
['__annotations__', '__builtins__', '__doc__', '__loader__', '__name__', '__package__', '__spec__']
>>> n=0
>>> dir()
['__annotations__', '__builtins__', '__doc__', '__loader__', '__name__', '__package__', '__spec__', 'n']
#Il simbolo 'n' è quello definito subito prima della seconda chiamata a dir().

#Se chiamata con un simbolo passato come parametro, dir resituirà il contenuto della tabella dei simboli associata a tale simbolo.
#Il simbolo: __builtins__ contiene le definizioni dei simboli predefiniti nell'interprete; tra questi vi è la funzione print.
#Utilizzando dir per visualizzare il contenuto della tabella dei simboli associata a tale modulo, l'output sarà il seguente:

>>> for e in dir(__builtins__):print(e)
... 
ArithmeticError
AssertionError
AttributeError
BaseException
BlockingIOError
BrokenPipeError
BufferError
BytesWarning
ChildProcessError
ConnectionAbortedError
ConnectionError
ConnectionRefusedError
ConnectionResetError
DeprecationWarning
EOFError
Ellipsis
EnvironmentError
Exception
False
FileExistsError
FileNotFoundError
FloatingPointError
FutureWarning
GeneratorExit
IOError
ImportError
ImportWarning
IndentationError
IndexError
InterruptedError
IsADirectoryError
KeyError
KeyboardInterrupt
LookupError
MemoryError
ModuleNotFoundError
NameError
None
NotADirectoryError
NotImplemented
NotImplementedError
OSError
OverflowError
PendingDeprecationWarning
PermissionError
ProcessLookupError
RecursionError
ReferenceError
ResourceWarning
RuntimeError
RuntimeWarning
StopAsyncIteration
StopIteration
SyntaxError
SyntaxWarning
SystemError
SystemExit
TabError
TimeoutError
True
TypeError
UnboundLocalError
UnicodeDecodeError
UnicodeEncodeError
UnicodeError
UnicodeTranslateError
UnicodeWarning
UserWarning
ValueError
Warning
ZeroDivisionError
__build_class__
__debug__
__doc__
__import__
__loader__
__name__
__package__
__spec__
abs
all
any
ascii
bin
bool
breakpoint
bytearray
bytes
callable
chr
classmethod
compile
complex
copyright
credits
delattr
dict
dir
divmod
enumerate
eval
exec
exit
filter
float
format
frozenset
getattr
globals
hasattr
hash
help
hex
id
input
int
isinstance
issubclass
iter
len
license
list
locals
map
max
memoryview
min
next
object
oct
open
ord
pow
print
property
quit
range
repr
reversed
round
set
setattr
slice
sorted
staticmethod
str
sum
super
tuple
type
vars
zip
"""