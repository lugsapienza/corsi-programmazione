"""
ESERCITAZIONE 'Il cifrario di Cesare'

Implementare la funzione: 'shift' che data in input una stringa: s ed un intero: n,
ritorni una nuova stringa contenente tutti i caratteri di s, dove ognuno è stato sostituito 
con l'n-esimo carattere successivo all'interno dell'alfabeto.

Esempi:
shift("abc", 1) -> "bcd"
shift("ciao", 1) -> "djbp"

Funzioni utili:
- ord(c, /) -> Return the Unicode code point for a one-character string.
- chr(i, /) -> Return a Unicode string of one character with ordinal i; 0 <= i <= 0x10ffff.

https://unicode-table.com/en/#basic-latin
"""

#SOLUZIONE:
def shift(s,n):
	"""
	Prende in input una stringa: s ed un intero: n.
	Ritorna una stringa dove ogni carattere di s è stato shiftato di n.
	Se s non è una stringa, oppure è una stringa vuota, la funzione ritorna il valore: None.
	Se n non è un intero, la funzione ritorna il valore: None.
	"""
	if not type(s) == str or not s or not type(n) == int: return None
	s1=""
	for i in s: s1 += chr(ord(i)+n)
	return s1
	


#UTILIZZO:
testo_originale = "io sono Mario"
chiave_di_cifratura = 23

print("Testo in chiaro:", "'"+testo_originale+"'")

testo_cifrato = shift(testo_originale, chiave_di_cifratura)

print("Testo cifrato:", "'"+testo_cifrato+"'")


print("Testo decifrato:", "'"+shift(testo_cifrato, chiave_di_cifratura * -1)+"'")
