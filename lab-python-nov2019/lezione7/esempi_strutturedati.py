s = {"Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato", "Domenica"}
print(type(s)) #<class 'set'>
print(s[0]) #TypeError: 'set' object is not subscriptable
print(s.pop())
print(s)
print(s.pop())
print(s.pop())
print(s)
print(s.pop())
print(s)

s=set(["ciao","mamma"])
print(s)
s=set("ciaomamma")
print(s)
s=set("ciaomamma come stai?")
print(s)
s=set(range(9))
print(s)
print(list(s))
print(list(s)[0])

print(list(range(list(s)[0]+6))[-1])
print(s)
s=set(["ciao",123,True])
print(s)
print(type(s.pop()))
print(type(s.pop()))
print(type(s.pop()))
print(type(s.pop())) #KeyError: 'pop from an empty set'
print(s is False)
if s: print(True)

s=set(["ciao",123,True])
if s: print(True)
s.clear()
if not s: print(False)

settimana = {"Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato", "Domenica"}
print("Sabato" in settimana)
print("sabato" in settimana)
giorno=settimana.pop()
print(giorno)
print(settimana)
print(settimana ^ giorno) #TypeError: unsupported operand type(s) for ^: 'set' and 'str'
print(settimana ^ set(giorno))
settimana={'Lunedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Martedì', 'Domenica'}
print(settimana)
print(settimana ^ {giorno})
print(settimana - giorno) #TypeError: unsupported operand type(s) for -: 'set' and 'str'
print(settimana - {giorno})
settimana.add(giorno)
print(settimana)
print(len(settimana))
settimana.remove("Lunedì")
print(settimana)
settimana.clear()
print(settimana)
settimana={'Lunedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Martedì', 'Domenica'}
print(settimana <= {"Sabato","Domenica"})
print(settimana < {"Sabato","Domenica"})
print(settimana > {"Sabato","Domenica"})
print(settimana >= {"Sabato","Domenica"})
print(settimana)
print(settimana >= {'Lunedì', 'Mercoledì', 'Giovedì'})
print(settimana <= {'Lunedì', 'Mercoledì', 'Giovedì'})
print(settimana < {'Lunedì', 'Mercoledì', 'Giovedì'})
print(settimana > {'Lunedì', 'Giovedì','Mercoledì'})
print(settimana >= {'Lunedì', 'Giovedì','Domenica'})
print(settimana)
print(settimana | {"Sabato"})
print(settimana ^ {"Sabato"})
print(settimana & {'Lunedì', 'Giovedì','Domenica'})
print(settimana)

l=[1,2]
print(l)
l.extend([3,4])
print(l)

rubrica = {'Mario': '5764584385', 'Luca': '5664554390'}
print('5764584385' in rubrica)
print('Mario' in rubrica)