"""
Il seguente esempio simula un programma che tenta di acquisire un valore numerico da tastiera,
richiedendolo nuovamente ad ogni inserimento errato, nullo o pressione dei tasti: CTRL-C/CTRL-D.
"""

l = []
while True:
  try: 
      n = int(input("Inserisci un numero: "))
      l[0] = n #Il primo elemento non esiste; verrà generato un IndexError. Cosa succederebbe se questa riga non ci fosse?
      print("...istruzioni dopo int(input())...")
      break
  except (ValueError, KeyboardInterrupt): print("\nValore errato o CTRL-C premuto")
  except IndexError as err: print("\nIndexError:",err)
  except: print("\nErrore generico")

  print("...istruzioni dopo il blocco try-except...")

print("...istruzioni dopo il while...")




"""
Questo esempio mostra cosa potrebbe succedere se venisse eseguito un controllo generico 
di tutte le tipologie di eccezioni che un certo blocco di codice potrebbe generare,
invece di optare per controlli più specifici(una except per ogni tipo di eccezione).
"""

from sys import exit 
l = []
try: 
    l[0] #Il primo elemento non esiste; verrà generato un IndexError. Provare a commentarla.
    print("..sto per chiamare la funzione: sys.exit..")
    exit(0) #Il programma non terminerà a causa di questa exit, perché l'eccezione da essa lanciata per terminare l'applicazione, verrà gestita dal ramo except(generico).
    print("..ho chiamato la funzione: sys.exit.")
except: print("ERRORE!")

print("...programma ancora in esecuzione...")