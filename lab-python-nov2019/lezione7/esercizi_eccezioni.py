"""
Dotare le seguenti funzioni di oppurtuni controlli per la correttezza dei rispettivi parametri di input,
lanciando opportune eccezioni nel caso in cui quest'ultimi non fossero corretti.
"""



#ESERCIZIO 1
def getElencoVocaboli(testo):
	"""
	Prende in input una stringa: 'testo' contenente parole separate da spazi, e ritorna una lista contenente tali parole.
	"""
	testo+=" "
	l=[]
	s=""
	for p in testo:
		if not p == " ":s+=p
		elif not s == "" and not s == " ":
			l+=[s]
			s=""
	return l




#ESERCIZIO 2
def rimpiazza(testo, vecchias, nuovas):
	"""
	Prende in input 3 stringhe: testo, vecchias e nuovas, e ritorna una lista contenente:
	- Come primo elemento, una stringa ottenuta da 'testo', sostituendo tutte le occorrenze di 'vecchias' con 'nuovas'.
	- Come secondo elemento, un intero corrispondente al numero di occorrenze che sono state sostituite.
	- Come terzo elemento, l'elenco degl'indici dove sono state trovate le occorrenze (l'indice della prima lettera di ogni occorrenza).
	Se 'vecchias' NON dovesse comparire in testo, ritornerà solo la stringa originale(anche se vuota).
	"""
	idxs=[]
	occ=0
	ln=len(vecchias)
	for i in range(len(testo)):
		if testo[i:i+ln] == vecchias:
			idxs+=[i]
			occ+=1
			tmp=testo[:i]+nuovas+testo[i+ln:]
			testo=tmp
	if not occ: return testo
	return [testo,occ,idxs]




#ESERCIZIO 3
def strENum(s):
	"""
	Prende in input una stringa: 's' contenente parole e numeri(solo interi) separati da spazi, e ritorna una lista contenente:
	- Come primo elemento, la lista delle sole parole(sottostringhe) contenute nella stringa; lista vuota se non presenti.
	- Come secondo elemento, una lista dei soli numeri interi(sottostringhe convertite ad intero) presenti nella stringa; lista vuota se non presenti.
	- Se 's' è vuota, la funzione restituirà una lista vuota (e non una lista di liste vuote).
	"""
	s+=" "
	parole=[]
	numeri=[]
	isnum=False
	t=""
	for p in s:
		if p in ["0","1","2","3","4","5","6","7","8","9"]:
			if not isnum: 
				t=""
				isnum=True
			t+=p
		elif not p == " ":
			if isnum: 
				t=""
				isnum=False
			t+=p
		elif not t == "" and not t == " " and isnum:
			numeri+=[int(t)]
			t=""
		elif not t == "" and not t == " " and not isnum:
			parole+=[t]
			t=""
	if not parole and not numeri: return []
	return [parole,numeri]




#ESERCIZIO 4
def alteraLista(dest, sorg, da, a):
	"""
	Prende in input 2 liste: 'dest', 'sorg' e due interi 'DA', 'A', e ritorna una NUOVA lista ottenuta da 'dest', sostituendo gli elementi dall'indice 'DA', all'indice 'A', con tutti quelli presenti in 'sorg'.
	Gli elementi nell'intervallo 'DA'-'A' saranno eliminati ed al loro posto ci saranno tutti gli elementi contenuti in 'sorg'.
	"""
	return dest[:da]+sorg+dest[a:]




#ESERCIZIO 5
def filtraERaggruppa(tipi, ogg):
	"""
	Prende in input 2 liste: 
	- 'tipi': contenente un elenco di tipi (int, float, str, list, ecc..).
	- 'ogg': contenente oggetti di varie tipologie.
	Ritorna una NUOVA lista contenente gli oggetti in 'ogg', ordinati per tipologia, secondo l'ordine in cui, tali tipologie, compaiono in 'tipi'.
	Se in 'ogg' sono presenti oggetti di una tipologia non elencata in 'tipi', quest'ultimi NON compariranno nella lista risultante.
	"""
	l=[]
	for t in tipi:
		for o in ogg:
			if type(o) == t: l+=[o]
	return l
