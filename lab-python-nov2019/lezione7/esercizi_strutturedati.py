"""
I seguenti esercizi sono stati creati per prendere dimestichezza con le operazioni ed i metodi disponibili di default
nei tipi base di Python.
E' quindi consigliato produrre implementazioni che facciano esclusivamente uso di tali tipologie di istruzioni,
senza ricorrere all'utilizzo di cicli o istruzioni condizionali(se non esplicitamente indicato nella traccia del esercizio).

Al link seguente sarà possibile trovare un elenco dei metodi disponibili per ogni tipo di dato, 
che possono essere utilizzati per la risoluzione degli esercizi:

https://docs.python.org/3.7/library/stdtypes.html
"""




#ESERCIZIO 1
"""
Data una lista: l, contenente una sequenza numerica, estendere tale sequenza di un fattore: n;
cioè aggiungere altri n elementi, alla lista originale, i quali valori partano da quello successivo all'ultimo presente in l.

Portare a termine l'esercizio senza l'utilizzo di costrutti condizionali o ciclici.
"""
l = [1,2,3]
n = 20

print("\n----Esercizio1----\nLista input: ",l,"\nn: ",n,sep="")

#-----------------
#-->SCRIVI SOLUZIONE QUI<--
#-----------------

print("\nRisultato: ",l,"\n",sep="")




#ESERCIZIO 2
"""
Data un stringa: s, contenente parole separate da spazi, generare una nuova stringa: s1, 
dalla quale sono state rimosse eventuali parole duplicate presenti in s.

Portare a termine l'esercizio senza l'utilizzo di costrutti condizionali o ciclici.

N.B.: Le parole contenute all'interno della stringa s, NON sono correlate fra loro; il loro ordine non conta.
"""
s = "se qualcosa non ti torna, hai qualcosa da chiedere"

s1 = ""

#-----------------
#-->SCRIVI SOLUZIONE QUI<--
#-----------------

print("\n----Esercizio2----\nStringa input: '",s,"'\nRisultato: '",s1,"'\n",sep="")




#ESERCIZIO 3
"""
Data un stringa: s, contenente parole separate da spazi, generare una nuova stringa: s1, 
dalla quale sono state rimosse le eventuali parole duplicate presenti in s.

In questa versione è possibile utilizzare, oltre agli operatori di base ed alle funzioni di libreria, 
anche i costrutti ciclici(ma NON il costrutto: IF-ELIF-ELSE).

La soluzione a questa variante del precedente problema dovrà preservare l'ordinamento delle parole contenute nella stringa originale, 
eliminando solo quelle duplicate.
"""
s = "se qualcosa non ti torna, hai qualcosa da chiedere"

s1 = ""

#-----------------
#-->SCRIVI SOLUZIONE QUI<--
#-----------------

print("\n----Esercizio3----\nStringa input: '",s,"'\nRisultato: '",s1,"'\n",sep="")




#ESERCIZIO 4
"""
Data una stringa: s, un carattere: c(stringa lunghezza 1), ed un separatore: sep(stringa lunghezza 1), produrre una lista: l 
contenente le sole parole, di s, che iniziano per c, dove le parole vengono separate tramite il carettere: sep.

Portare a termine l'esercizio senza l'utilizzo di costrutti condizionali o ciclici.

E' possibile utilizzare i soli operatori base ed i metodi standard messi a disposizione dagli oggetti stringa e lista, in più, le seguenti funzioni:
- ord(..)
- chr(..)

Utilizzare: help(ord) o help(chr) per capirne il funzionamento.
"""
s = 'ciao mamma guarda come mi diverto.'
c = "m"
sep = " "

l = []

#-----------------
#-->SCRIVI SOLUZIONE QUI<--
#-----------------

print("\n----Esercizio4----\nStringa input: '",s,"'\nFiltrare per carattere: '",c,"'\nSeparatore: '",sep,"'\nRisultato: ",l,"\n",sep="")
