from sys import exit
if __name__ == "__main__": exit("Questo modulo non può essere utilizzato come uno script")

from os import name as osversion
from os import system
if osversion == "nt": import winsound as ws #Per le piattaforme Windows è disponibile un modulo per le funzionalità sonore.

#Definizione note musicali
Nota = {"DO": 262, "DOd": 277, "RE": 294,"REd": 311, "MI": 330, "FA": 349, "FAd": 370, "SOL": 392, "SOLd": 415, "LA": 440, "LAd": 466, "SI": 494}
Durata = {"INTERO": 1600, "MEZZO": 800, "QUARTO": 400, "OTTAVO": 200, "SEDICESIMO": 100}

#Definizione funzionalità
def suona(nota, durata):
	"""
	Prende in input la frequenza della nota da emettere(intero), e la durata della nota(intero).
	Emette un suono alla frequenza e durata specificata, utilizzando il beep di sistema.

	Ritorna un valore nullo in caso di successo; valore non nullo altrimenti.

	Lancia l'eccezione: TypeError, se il tipo dei parametri non è corretto.
	"""
	if not type(nota) == int or not type(durata) == int: raise TypeError("'nota' e 'durata' devono essere due interi")

	if osversion == "nt": return ws.Beep(nota,durata)
	elif osversion == "posix": return system('play --no-show-progress --null --channels 1 synth %s sine %f' % (durata/1000, nota))

def note_disponibili():
		"""
		Ritorna una lista contenente le stringhe corrispondenti alle note disponibili.
		"""
		return list(Nota)
