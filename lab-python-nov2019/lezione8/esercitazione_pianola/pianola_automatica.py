"""
Implementare uno script che permetta, utilizzando le funzionalità definite nel modulo: suoni,
di suonare note lette da un file passato come parametro al programma.
Il file in questione dev'essere di tipo testuale e, deve contenere una nota(una delle stringhe restituite dalla funzione: note_disponibili) per ogni riga, oppore una riga vuota.
Ogni riga vuota dev'essere interpretata come una pausa di 0.6 secondi tra l'emissione dell'ultima nota, e l'emissione della successiva.

Terminata la riproduzione delle note, il programma dovrà mostrare un messaggio di saluto prima di terminare.

Funzioni utili:
- sleep(s), definita nel modulo standard: time
- suona(nota, durata), definita nel modulo: suoni
- note_disponibili(), definita nel modulo: suoni


Esempio d'esecuzione:

python3 pianola_automatica.py brano.txt   #'brano.txt' è il nome del file contenente le note(una per riga o riga vuota).
"""


#SOLUZIONE:


#FUNZIONI UTILI
def leggi_note(nomefile, sep = "\n"):
	"""
	Prende in input il nome di un file(nomefile) ed, eventualmente, una stringa di separazione(sep) e, 
	restituisce una lista contenente tutte le note presenti nel file, 
	utilizzando la stringa di separazione per distinguere le note.

	Genera un ValueError, in caso di parametri errati o nulli.
	In caso di errori durante l'apertura o la lettura del file, rilancerà l'eccezione generata.
	"""
	if not type(nomefile) == str or nomefile == "": raise ValueError("'nome' dev'essere una stringa non vuota")
	if not type(sep) == str or sep == "": raise ValueError("'sep' dev'essere una stringa non vuota")
	note = []
	try:
		f = open(nomefile)
		note = f.read().split(sep)
		f.close()
	except: raise
	return note


immagineAlbero = """
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM,MMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMWd,,dWMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMWOc,.  .,cOWMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMNo.    .oNMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMO;;cc;;OMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMNX0kkKXNMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMKo.  .oXMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMXo.      .oXMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMXo.          .oKMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMWXo.              .oXMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMXo.                  .oXMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMNx,.                    .,xNMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMNKkl.                  .lO0NMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMWO;.                     :OWMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMWO:.                        .:OWMMMMMMMMMMMMMM
MMMMMMMMMMMMNO:.                            .:OWMMMMMMMMMMMM
MMMMMMMMMMMMNkdddo'                      'odddkNMMMMMMMMMMMM
MMMMMMMMMMMMMMMNk;                        ;kNMMMMMMMMMMMMMMM
MMMMMMMMMMMMMNk;                            ;kNMMMMMMMMMMMMM
MMMMMMMMMMMNk;                                ;kNMMMMMMMMMMM
MMMMMMMMMNk;                                    ;kNMMMMMMMMM
MMMMMMMMMKdlllll,                          ,llllldKWMMMMMMMM
MMMMMMMMMMMMMW0c.                          .c0WMMMMMMMMMMMMM
MMMMMMMMMMMW0c.                              .c0WMMMMMMMMMMM
MMMMMMMMMW0c.                                  .c0WMMMMMMMMM
MMMMMMMWOc.                                      .c0WMMMMMMM
MMMMMMMXxooooooooooooooooo,      ,oolloooooooooooooxXMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMWl      cWMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMM0,      '0MMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMx.      .dMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMWc        lWMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
"""
testoAuguri = """
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::BUON:NATALE:DAL:LUG:SAPIENZA:_::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
"""


#CORPO DEL PROGRAMMA
from suoni import Nota as note
from suoni import suona
import time
import sys

if not len(sys.argv) == 2: sys.exit("\nUtilizzo: python3 pianola_automatica.py [FILE_DA_SUONARE.txt]\n")

#Leggi note dal file specificato
brano = []
try: brano = leggi_note(sys.argv[1])
except BaseException as errore: sys.exit(errore)

print(immagineAlbero)

#Suona le note
for nota in brano:
	if nota == "": time.sleep(0.6) #Le pause, tra una o più note, vengono rappresentate mediante una stringa vuota(riga vuota all'interno del file). In caso di pause, il programma non farà nulla per 0.6 secondi.
	else:
		if not nota in note: sys.exit("Errore: La nota '"+nota+"' non esiste.\n")
		if suona(note[nota], 550): #Questa funzione ritorna un codice d'errore, diverso da 0 nel caso in cui l'emissione del suono non dovesse andare a buon fine. 
			sys.exit("\n!! Installare 'sox' per eseguire questo script su macchine Linux o Mac !!\n   Provare con: sudo apt install sox\n")

print(testoAuguri)
