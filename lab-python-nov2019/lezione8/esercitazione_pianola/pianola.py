"""
Implementare uno script che permetta, utilizzando le funzionalità definite nel modulo: suoni,
di suonare delle note inserite da tastiera.

Il programma dovrà dare all'utente la possibilità di inserire, da tastiera, una sequenza di note separate da spazi
(ad ogni spazio aggiuntivo, tra una nota e la successiva, il programma dovrà fare una pausa di 0.6 secondi, prima di suonare la nota successiva),
che verranno suonate, in successione, alla pressione del tasto: INVIO.

Terminata la riproduzione delle note, il programma dovrà chiedere un nuovo inserimento.
Nel caso in cui l'utente volesse uscire dal programma, dovrà avere la possibilità di farlo premendo la combinazione di tasti: CTRL+C.

Il programma dovrà mostrare un messaggio di saluto prima di terminare.

Funzioni utili: 
- sleep(s), definita nel modulo standard: time
- suona(nota, durata), definita nel modulo: suoni
- note_disponibili(), definita nel modulo: suoni
"""

#SOLUZIONE:


#FUNZIONI UTILI:
def estrai_brano(stringa):
    if not type(stringa) == str: raise TypeError("'stringa' dev'essere una stringa")
    if stringa == "": return []
    note = []
    nota = ""
    separato = False
    stringa = stringa.strip()
    for c in stringa+" ":
        if not c == " ":
            separato = False
            nota += c
        elif c == " " and not separato:
            note += [nota]
            nota = ""
            separato = True
        else: note += [""]
    return note


#MAIN:
from suoni import suona, note_disponibili, Nota as note
from time import sleep
from sys import exit

print("\n---Benvenuto in PYnola---\nNote disponibili:",note_disponibili(),"\n")

while True:
    dasuonare = ""
    try: dasuonare = input("Inserisci le note: ")
    except KeyboardInterrupt as sig:
        print("\nGrazie per aver suonato con PYnola\n")
        break
    except BaseException as eccezione: exit("\nErrore generico: "+str(eccezione)+"\n")

    for nota in estrai_brano(dasuonare):
        if nota == "": sleep(0.6) #Le pause, tra una o più note, vengono rappresentate mediante una stringa vuota(spazi consecutivi). In caso di pause, il programma non farà nulla per 0.6 secondi (time.sleep()).
        else:
            if not nota in note: exit("Errore: La nota '"+nota+"' non esiste.\n") #La nota viene utilizzata come chiave per estrarne la frequenza corrispondente, dal dizionario. Bisogna essere certi che la chive esista, prima di estrarne il valore. 
            if suona(note[nota], 550): #Questa funzione ritorna un codice d'errore, diverso da 0, nel caso in cui l'emissione del suono non dovesse andare a buon fine.
                exit("\n!! Installare 'sox' per eseguire questo script su macchine Linux o Mac !!\n   Provare con: sudo apt install sox\n")