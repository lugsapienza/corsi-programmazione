#QUESTO E' UN COMMENTO(INIZIA PER '#' E FINISCE ALLA FINE DELLA RIGA); VIENE IGNORATO DALL'INTERPRETE



#----ALLOCAZIONE IN MEMORIA E TIPI NUMERICI----

#RICORDARE: Ogni tipo di dato, in Python, è un oggetto; in grado di mantenere una precisa tipologia 
#d'informazione, e che può essere coinvolto in specifiche operazioni definite dal suo creatore
#(il creatore della struttura dati), quali, per esempio, somma e sottrazione per un oggetto numerico.

n = 3 #Allocazione di un intero, tramite assegnazione al simbolo: n
fn = 4/3 #Allocazione di un float, come risultato dell'operazione di divisione(restituisce sempre un float)

#RICORDARE: Un float occupa più spazio in memoria, rispetto ad un intero.
#Python non è un linguaggio fortemente tipizzato; ciò vuol dire che egli è in grado di
#capire il tipo del oggetto associato ad un certo simbolo, semplicemente leggendo il tipo stesso.
#Tale meccanismo gli permette anche di capire quanta memoria assegnare per ciascun oggetto.
#La quantità di memoria che l'interprete è in grado di assegnare, per in certo tipo di dato, 
#dipende della specifiche di una certa macchina sulla quale il programma deve girare,
#ma tutto questo lavoro viene fatto automaticamente dall'interprete.

fin=4//3 #Divisione nella quale viene presa SOLAMENTE la parte intera.
#Osservare, però, i seguenti esempi:
4//3.3 #Risultato: 1.0
4//3 #Risultato: 1
4/3 #Risultato: 1.3333333333333333

#Come è possibile notare, la divisione con '//', tra un intero ed un float, resituirà SEMPRE UN FLOAT, 
#avente, però, la parte decimale TUTTA A 0; al contrario, invece, della medesima divisione standard '/',
#che produce una parte decimale pari a: 0.3333333333333333, come visibile nell'ultimo esempio.


#RICORDARE: Una divisione, con solo la parte intera, tra due operandi DI TIPO INTERO, 
#che in una normale divisione restituirebbe un numero decimale, resituirà, invece, un numero intero(1).
3+4J #Definizione di un numero complesso, NON assegnato ad un simbolo (non essendo raggiungibile, 
#dopo la sua definizione, verrà automaticamente distrutto dall'interprete).

comp=4+7j
comp.real #Accesso, in lettura, alla parte reale.
comp.imag #Accesso, in lettura, alla parte immaginaria.

#In memoria, un complesso, occupa lo spazio corrispondente a due float(entrambe le parti sono dei float).


2 > 2+3j #La comparazione in maggioranza o minoranza, tra un intero ed un complesso, genererà un errore
#di incompatibilità dei tipi.
2 == 2+3j #La comparazione in uguaglianza è invece possibile.



#----LA SEQUENZA: STRINGA----

s = "ciao" #Definizione di una sequenza di caratteri(oggetto stringa).

#In memoria: [s] -> [c|i|a|o]    Una stringa è una sequenza di locazioni, 
#dove il riferimento alla prima locazione viene associato al simbolo(s) che compare a sinistra del =.


s #Accesso, in lettura, alla stringa s.
s[0] = "m" #Accesso in scrittura(riassegnazione) all'elemento 0 di s.

#RICORDARE: Essendo, la stringa, un oggetto immutabile, è possibile leggerla o copiarla,
#ma non è possibile eseguire operazioni di scrittura(modifiche) su di essa.


s="m"+s[1:] #Modifica di s, da "ciao" a "miao".

#La stringa s originale viene sostituita(distrutta) dalla nuova stringa 
#ottenuta tramite la concatenazione di "m" con la sottostringa che va dall'indice 1, fino alla fine di s.

s*2 #Restituisce la stringa: "ciaociao".

s[1:3:2] #Restituisce la stringa: "i" (slicing con: partenza, arrivo e salto).

s=0 #La stringa, associata ad s, viene distrutta; al suo posto ci sarà l'intero: 0.



#----LA SEQUENZA: LISTA----

l=[1,2,3,4]

l[0] #Restituisce il primo elemento di l.
l[::2] #Restituisce una lista contenente gli elementi in POSIZIONE PARI.
[9]*4 #Genera la lista: [9,9,9,9]
l[len(l):]=[5,6,7,8] #Assegna la lista [5,6,7,8], alla fine di l(dalla fine, in poi), estendendola.
[[1,2],[3,4]] #Lista di due elementi, dove quest'ultimi sono liste di due elementi(una matrice 2x2).
[[1,2],[3,4]][0] #Restituisce la sottolista: [1,2].
[[1,2],[3,4]][1][1] #Restituisce l'elemento: 4, contenuto nella sottolista: [3,4].


#----STAMPA A VIDEO DI UN OGGETTO----

print(l) #Stampa a video l.
print(s,comp,fin,fn,n) #Stampa gli oggetti elencati fra parentesi, inserendo uno spazio vuoto tra una stampa e l'altra.