#ESERCIZIO 1
"""
Data una stringa: s contenente parole separate da spazi, produrre una lista: l contenente le sole parole in posizione pari.

Portare a termine l'esercizio senza l'utilizzo di costrutti condizionali o ciclici.
NOTE: s.split() restituisce una lista contenente tutte le sottostringhe che, in s, sono separate da uno spazio.
"""

s = "Parola0 parola1 parola2 parola3 parola4"

l = []


#----SOLUZIONE----
l = s.split()[::2]
#-----------------



#ESERCIZIO 2
"""
Data la lista: l=["ciao",'mamma',123,0.,.0]
Cosa restituisce la seguente espressione: [l[2],l[:]*2][1][2]*2
?

Rispondere senza utilizzare l'interprete.
"""


#Risposta:
#l[2] => 123
#l[:] => ["ciao",'mamma',123,0.,.0] (una copia della lista)
#[l[2],l[:]*2] => [123,['ciao', 'mamma', 123, 0.0, 0.0, 'ciao', 'mamma', 123, 0.0, 0.0]]
#[123,['ciao', 'mamma', 123, 0.0, 0.0, 'ciao', 'mamma', 123, 0.0, 0.0]][1] => ['ciao', 'mamma', 123, 0.0, 0.0, 'ciao', 'mamma', 123, 0.0, 0.0]
#['ciao', 'mamma', 123, 0.0, 0.0, 'ciao', 'mamma', 123, 0.0, 0.0][2] => 123
#123 * 2 => 246



#ESERCIZIO 3
"""
Data la stringa: s="ciao"
Cosa restituisce la seguente espressione: s[-1]+s[1:-1][-1]+s[1:-2][0]+s[0]
?

Rispondere senza utilizzare l'interprete.
"""


#Risposta:
#s[-1] => "o"
#s[1:-1] => "ia"
#s[1:-1][-1] => "a"
#s[1:-2] => "i"
#s[1:-2][0] => "i"
#s[0] => "c"
#s[-1] + s[1:-1][-1] + s[1:-2][0] + s[0] => "oaic"