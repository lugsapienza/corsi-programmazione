#----IF e VALORI DI VERITA'----

n=0
if n>0:print("n > 0") #n = 0, la print non viene eseguita.

n=1
if n>0:print("n > 0") #n = 1 => n > 0, la print viene eseguita.

if    n>0:print("n > 0") #tra la parola 'if' e l'espressione booleana, dev'esserci almeno uno spazio; 
#più spazi consecutivi non saranno presi in considerazione.

ifn > 0:print("n > 0") #Non essendoci lo spazio tra 'if' e la condizione booleana, l'interprete non è in grado di riconoscere l'istruzione if.
  #File "<stdin>", line 1
#SyntaxError: illegal target for annotation

#Il booleano può essere il valore True o False inserito direttamente nell'if, oppure un'espressione booleana:
if True: print("blocco eseguito")
if 1: print("blocco eseguito")
if -3: print("blocco eseguito")
if "ciao": print("blocco eseguito")
if [1]: print("blocco eseguito")
#Inserire un oggetto di tipo numerico, stringa o lista, equivale ad inserire il valore: True, 
#nel caso in cui tale oggetto numerico non sia 0, o tale oggetto sequenza non sia vuoto(nullo).

if False: print("blocco eseguito")
if 0: print("blocco eseguito")
if "": print("blocco eseguito")
if []: print("blocco eseguito")
#Allo stesso modo, inserire un oggetto numerico, o sequenza, avente valore nullo, 
#equivale ad inserire il valore: False.



#----ELIF----

n=1
if n > 0: print("n > 0")
elif n == 0: print("n = 0")
n=0
if n > 0: print("n > 0")
elif n == 0: print("n = 0")


if n > 0: 
    print("n>0")
elif n==0:
print("n=0") #Questa print si trova fuori dal blocco elif, a causa della scorretta indentazione.
#L'interprete generera' un errore a causa del fatto che tale blocco elif non contiene istruzioni
#(non essendo indentato correttamente, non c'è nessuna istruzione che faccia parte del blocco).


if n > 0 and not print("print() eseguita"): print("n>0")
#In questo blocco if vogliamo avere la prova che solo la prima delle due condizioni poste in and, 
#venga effettivamente valutata, nel caso in cui quest'ultima sia falsa.
#Per effettuare questa prova, tale condizione è stata messa in and con la chiamata ad una print,
#(così da poterne osservare l'eventuale esecuzione) 
#il quale valore ritornato(None) viene posto a True per mezzo della negazione.
#Se tale print dovesse essere eseguita, quanto detto in precedenza non sarebbe vero.



#----FOR----

for n in range(11):
    if n%2: continue
    print(n) #Stampa dei soli numeri pari, da 0 a 10.


for n in range(11):
    #if n==5: break
    print(n)
else: print("Tutti i numeri") 
#La clausola else, correlata ad un ciclo, verrà eseguita solamente se tutte le iterazioni di tale ciclo
#saranno state eseguite.
#Tutto ciò è possibile farlo sia su cicli while, che for.