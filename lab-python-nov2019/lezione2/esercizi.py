"""
ESERCIZIO 1

Data una lista l eterogenea, iterare su di essa rimpiazzando eventuali elementi nulli con UNA COPIA della lista stessa.

Esempio:
Lista iniziale: l=['ciao', 'mamma', '', 'stai?']
Lista modificata: ['ciao', 'mamma', ['ciao', 'mamma', '', 'stai?'], 'stai?'].
"""

l=['ciao', 'mamma', '', 'stai?']
print("Esercizio 1:\n", l, sep="")

#-----------------
#-->SCRIVI SOLUZIONE QUI<--
#-----------------

print("\n", l, "\n", sep="")



"""
ESERCIZIO 2

Data una lista l = ['ciao', 1, (2+7j)]
Scorrerla utilizzando il while, stampandone i soli elementi di tipo int(interno) e str(stringa).

SUGGERIMENTO: La funzione: type(obj) restituisce il tipo di un oggetto obj,
dove il tipo può essere uno tra i seguenti: int, float, str, list, complex, bool, range.
Es: type("ciao") => str

Terminato l'esercizio, ripeterlo cambiando l.
esempi:  
l = [[], True, ""]
l = [0, 1, [1,2]]
l = []
"""

l=[["ciao"],[],"ciao",True,0]
print("Esercizio 2:\n", l, sep="")

#-----------------
#-->SCRIVI SOLUZIONE QUI<--
#-----------------

print("\n", l, "\n", sep="")



"""
ESERCIZIO 3 'La matrice delle occorrenze ':

Data una lista L di stringhe, creare una matrice M SIMMETRICA, 
contenente R righe quante sono le stringhe in L, 
e C colonne quanti sono i caratteri della stringa più lunga presente in L.
La matrice dovrà memorizzare, per i caratteri di ogni stringa, 
il conteggio delle loro occorrenze all’interno di essa, 
inserendo degli zeri per rendere la matrice simmetrica, 
nel caso in cui la stringa in esame dovesse avere meno caratteri rispetto a quella più lunga.


Esempio:

Lista: L = [“ieri”,”sono”,”stato”,”al”,”bagno”]
Risultato: M = [[2,1,1,2,0], [1,2,1,2,0], [1,2,1,2,1], [1,1,0,0,0], [1,1,1,1,1]]
"""

l = ["ieri","sono","stato","al","bagno"]
m=[]

#-----------------
#-->SCRIVI SOLUZIONE QUI<--
#-----------------

print("Esercizio 3:\n",l,"\n\n",m,sep="")



"""
ESERCIZIO 4 'L’inverter':

Data una lista: L di stringhe, produrre una nuova lista: Z contenente tutte le stringhe di L, invertite.


Esempio:

Lista: L = [“Sono”,”Anna”]
Risultato: Z = [“onoS”,”annA”]
"""

l=["Sono","Anna"]
z=[]

#-----------------
#-->SCRIVI SOLUZIONE QUI<--
#-----------------

print("\nEsercizio 4:\n",l,"\n\n",z,sep="")



"""
ESERCIZIO 5 'Il collassatore delle nullità':

Data una lista 'L' eterogenea, iterare su di essa rimpiazzando eventuali elementi nulli 
con UNA COPIA della lista stessa.
In caso di liste annidate contenenti anch’esse valori nulli, 
rimpiazzarli con la copia della rispettiva LISTA DI APPARTENENZA.


Esempio:

Lista iniziale: L = [1,2,["ciao",""],0]
Lista modificata: [1,2,["ciao",["ciao",""]],[1,2,["ciao",["ciao",""]],0]]
"""

l = [0,["",56],1] #Prova anche con: [1,2,["ciao",""],0]

print("\nEsercizio 5:\n",l,sep="")

#-----------------
#-->SCRIVI SOLUZIONE QUI<--
#-----------------

print("\n",l,sep="")
			
