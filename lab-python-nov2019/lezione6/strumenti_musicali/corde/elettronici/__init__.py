iam="strumenti_musicali/corde/elettronici/__init__.py" #Aggiunta per distinguere i simboli di moduli differenti.
print("IAM",iam) #Aggiunta per capire se, e quando, questa init verrà eseguita.

__all__ = ["chitarra_elettrica"]

from . import * #introdotta per importare tutti i moduli definiti nel package(cartella) locale, nella tabella di questo modulo
#(i moduli di questo package verranno importati nella tabella di questo file, e non di quello che ha eseguito l'import del package).