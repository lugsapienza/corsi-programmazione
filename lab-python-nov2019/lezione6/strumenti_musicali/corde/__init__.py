iam="strumenti_musicali/corde/__init__.py" #Aggiunta per distinguere i simboli di moduli differenti.
print("IAM",iam) #Aggiunta per capire se, e quando, questa init verrà eseguita.

__all__ = ["arpa","chitarra","elettronici"]
#'elettronici' è una directory; a tale simbolo verrà associata la tabella dei simboli di elettronici/__init__.py
#Non verranno eseguiti gli import di tutti i moduli locali a tale directory, ma verrà solo eseguito il modulo init.