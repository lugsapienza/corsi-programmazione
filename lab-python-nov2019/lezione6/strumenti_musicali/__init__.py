iam="strumenti_musicali/__init__.py" #Aggiunta per distinguere i simboli di moduli differenti.
print("IAM",iam) #Aggiunta per capire se, e quando, questa init verrà eseguita.

#__all__ = ["strumento"] #Togliendo questa riga, varrà solo eseguito __init__.py, senza importare i moduli della cartella corrente.

__all__ = ["corde","fiato"] 
#Sto tentando di importare, a catena, i moduli definiti nei sotto-package: 'corde' e 'fiato', specificandone il nome in __all__.
#Quello che otterrò saranno le voci: 'corde' e 'fiato', nella tabella dei simboli, associati alla tabella dei simboli dei rispettivi __init__.py
#e non i simboli dei moduli in essi localizzati.