if __name__ == "__main__":
	print("""Questo modulo implementa le seguenti funzioni:
			- shiftPosizione(arg,i)
			- shiftPosizioneInversa(arg,i)
		Utilizzo: import utilita.py
		""")
else:
	def shiftPosizione(arg, i):
		"""
		Questa funzione prende in input una stringa 'arg' ed un intero 'i' 
		e sposta a SINISTRA di 'i' posizioni i caratteri contenuti in 'arg', 
		ritornando la nuova stringa in output.
		Effettuare oppurtuni controlli sui parametri di input.
		
		Esempi:
		shiftPosizione("abc", 1) => "bca"
		shiftPosizione("abcde", 2) => "cdeab"
		"""
		
		if type(arg) != str or type(i) != int: return
		ret = arg[i:] + arg[0:i]
		return ret


	def shiftPosizioneInversa(arg, i):
		"""
		Questa funzione prende in input una stringa 'arg' ed un intero 'i' 
		e sposta a DESTRA di 'i' posizioni i caratteri contenuti in 'arg', 
		ritornando la nuova stringa in output.
		Effettuare oppurtuni controlli sui parametri di input.

		Esempi:
		shiftPosizione("abc", 1) => "cab"
		shiftPosizione("abcde", 2) => "deabc"
		"""
		
		if type(arg) != str or type(i) != int: return
		ret = arg[-i:] + arg[0:-i]
		return ret
