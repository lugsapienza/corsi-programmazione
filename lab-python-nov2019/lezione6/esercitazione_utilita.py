"""
ESERCITAZIONE SUI MODULI

Scrivere un modulo 'utilita' che possa essere utilizzato nelle seguenti due modalità:
1) script: Dovrà eseguire la print di una stringa che descriva le funzionalità del modulo.
		   Esempio:
            print(\"\"\"Questo modulo definisce i simboli:
				shiftPosizione(arg, i)
				shiftPosizioneInversa(arg, i)
			Utilizzo: import utilita.py
			\"\"\")
			
2) import: Dovrà definire i simboli sopra citati.
""" 

def shiftPosizione(arg, i):
	"""
	Questa funzione prende in input una stringa 'arg' ed un intero 'i' 
	e sposta a SINISTRA di 'i' posizioni i caratteri contenuti in 'arg', 
	ritornando la nuova stringa in output.
	Effettuare oppurtuni controlli sui parametri di input.
	
	Esempi:
	shiftPosizione("abc", 1) => "bca"
	shiftPosizione("abcde", 2) => "cdeab"
	"""
	pass


def shiftPosizioneInversa(arg, i):
	"""
	Questa funzione prende in input una stringa 'arg' ed un intero 'i' 
	e sposta a DESTRA di 'i' posizioni i caratteri contenuti in 'arg', 
	ritornando la nuova stringa in output.
	Effettuare oppurtuni controlli sui parametri di input.
	
	Esempi:
	shiftPosizione("abc", 1) => "cab"
	shiftPosizione("abcde", 2) => "deabc"
	"""
	pass
