"""
ACQUISIZIONE DATI DA TASTIERA CON: input() / AL PASSAGGIO DEI PARAMETRI TRAMITE: sys.argv


Scrivere un modulo che implementi una mini calcolatrice in grado di stampare
a video la somma di due operandi numerici inseriti da tastiera, o(a scelta) passati come parametro allo script.

Effettuare gli opportuni controlli per evitare che un utilizzo scorretto del programma
comporti la sua terminazione con errori.
"""

#SOLUZIONE:
def isNumber(arg):	
	"""
	Prende come parametro una stringa: arg.
	Ritorna True se arg rappresenta un numero naturale, False altrimenti.
	"""
	if not type(arg) == str: return False
	l = ["0","1","2","3","4","5","6","7","8","9"]
	#ciclo su ogni carattere per controllare se è un numero tra 0 e 9
	for c in arg:
		if c not in l: return False
	return True
	
#isNumber è una funzione di supporto che realizza una funzionalità utile per questo programma
#ma che non è strettamente legata ad esso(può essere utilizzata in altri programmi, senza la necessità di alterarne il codice).


a = input("Primo operando: ") 
#Il programma si blocca in attesa della pressione del tasto: INVIO.
#Alla pressione di quest'ultimo, la funzione ritornerà una stringa 
#contenente tutti i caratteri che sono stati digitati prima dell'INVIO.

b = input("Secondo operando: ")

#Controllo che le stringhe A e B rappresentino due numeri naturali, utilizzando la funzione: isNumber
if not isNumber(a) or not isNumber(b): print("\n   Uno dei due parametri inseriti NON e' un numero!\n\n")
else: print("\n Somma: ",int(a)+int(b),"\n\n") #Se lo sono, li converto ad intero e ne stampo la somma.
