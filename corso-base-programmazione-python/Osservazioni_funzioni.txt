>>> def f(arg):print(arg)
... 
>>> print(f("ciao"))
ciao
None    # => Valore di default causato dall'assenza del return.


>>> def f(arg):return print(arg)
... 
>>> print(f("ciao"))
ciao
None    # => Valore ritornato dalla funzione: print.


>>> k = f    # => Assegnazione indirizzo funzione f a k.
>>> k = f("ciao")    # => Assegnazione valore di ritorno di f a k.
ciao
>>> 
