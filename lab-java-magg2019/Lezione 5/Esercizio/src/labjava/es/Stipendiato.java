package labjava.es;

public interface Stipendiato {
    /*
    *  L'interfaccia stipendiato rappresenta una persona con uno stipendio
    **/

    /**
     * Ritorna lo stipendio della persona
     * **/
    int getStipendio();
}
