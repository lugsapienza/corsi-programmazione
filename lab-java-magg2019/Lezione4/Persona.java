public class Persona{
    String nome,cognome;
    int eta;


    public Persona(String nome, String cognome, int eta){
        this.nome = nome;
        this.cognome = cognome;
        if(eta < 0 || eta > 120) throw new IllegalArgumentException();
    }


    @Override
    public int hashCode(){}

    @Override
    public boolean equals(Object oth){
        try{
            Persona altra = ((Persona) oth);
            //TODO implementare il resto dell'equals quì;
        }
        catch(Excpetion e){
            return false;
        }
    }


    /**
    Implementare equals ed hashCode in modo tale che valgano le seguenti proprietà:
    1) p1.equals(p2) <--> p1.nome.equals(p2.nome) && p1.cognome.equals(p2.cognome) && p1.eta == p2.eta
    2) p1.equals(p2) == true --> p1.hashCode() == p2.hashCode();
    NOTA: Richiede un main esterno
    **/
}
