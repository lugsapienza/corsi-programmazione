<h2>Esercizio 1</h2>
Scrivere un programma Java che utilizzi due cicli "for" per stampare il seguente output:
<br>
*******
<br>
******
<br>
*****
<br>
****
<br>
***
<br>
**
<br>
*

<br>
<br>
<h2>
Esercizio 2</h2>
Scrivere un programma Java che prenda ripetutamente in input un intero e ne stampi la media. L'input termina all'inserimento di 0.

Esempio
Input: 5 10 2 4 0
Output: 5.25

NB L'output deve essere di tipo double.
<br>
<br>
<h2>
Esercizio Bonus</h2><br>
Advanced Guessing Game<br>
Scrivere un programma Java che generi un numero random tra 1 e 3. Il giocatore dovrà inserire un valore compreso tra 1 e 3. Nel caso in cui il numero inserito sia lo stesso del numero generato, il giocatore guadagna un punto, in caso contrario, il computer guadagna un punto. 
Tra una "partita" e l'altra il giocatore può decidere se continuare o no la partita.
<br><br>
<b>NB1</b> Non utilizzare il metodo <code>input.nextInt()</code>.
Alternativa:
La funzione <code>Integer.parseInt("1")</code> restituisce l'intero all'intero di una stringa.

Es.
<code>int numero = Integer.parseInt("1");</code>

numero conterrà il valore 1.
<br>


<b>NB2</b> La generazione del numero random questa volta si può ottenere con la seguente istruzione.

<code>int random = (int)((Math.random()*10)%3)+1;</code>

random conterrà un valore tra 1 e 3.

Esempio di output:<br>
![alt text](lab-java-nov2018/Lezione4/Resources/output.jpg)
