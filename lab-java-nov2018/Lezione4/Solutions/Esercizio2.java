
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author eric
 */
public class Esercizio2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double somma = 0;
        int quanti = 0;
        while(true){
            System.out.println("Inserisci un numero intero diverso da 0: ");
            int number = Integer.parseInt(in.nextLine());
            if(number==0){
                break;
            }
            somma+=number;
            quanti++;
        }
        if(somma==0||quanti==0){
            System.out.println("Errore. Sono stati inseriti 0 numeri oppure la media è 0.");
        }else{
            double media = somma/quanti;
            System.out.println(media);
        }
    }
}
