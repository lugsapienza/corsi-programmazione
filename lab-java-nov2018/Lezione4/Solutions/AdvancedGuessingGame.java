/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Scanner;

/**
 *
 * @author eric
 */
public class AdvancedGuessingGame {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
       
        //inizializzo i punti
        int puntiGiocatore = 0;
        int puntiComputer = 0;
        //Creo un booleano che modificherò a false nel caso in cui voglia fermare il gioco.
        boolean continua = true;
        while(continua){
            System.out.print("Inserisci un numero tra 1 e 3: ");
            int inserito = Integer.parseInt(input.nextLine());
            if(inserito<1||inserito>3){
                System.out.println("Devi inserire un numero tra 1 e 3!");
                continue;
            }
            int random = (int)((Math.random()*100)%3)+1;
            if(inserito==random){
                System.out.println("Hai indovinato!");
                puntiGiocatore++;
            }else{
                System.out.println("Hai sbagliato!");
                puntiComputer++;
            }
            System.out.println("Il numero era "+random+".");
            System.out.println("Punteggio\nTu: "+puntiGiocatore+" Computer: "+puntiComputer);
            //Conferma se continuare
            while(true){
                System.out.print("Vuoi continuare? (s/n) ");
                String conferma = input.nextLine();
                //Se continuo esco dal ciclo.
                if(conferma.equals("s")){
                    break;
                }else{
                    //Se è uguale no, finisco il ciclo esterno e esco da questo ciclo
                    if(conferma.equals("n")){
                        continua = false;
                        break;
                    }
                }
            }
        }
        
    }
}
