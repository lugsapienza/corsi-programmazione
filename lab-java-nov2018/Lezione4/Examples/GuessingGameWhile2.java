
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author eric
 */

public class GuessingGameWhile2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int random = (int) (Math.random()*10)+1;    
        int i = 0;
        while(i<3){
            if(input.nextInt()==random){
                System.out.println("Hai vinto!");
                break;
            }
            if(i==2){
                System.out.println("Hai sbagliato, il numero era "+random);
            }else{
                System.out.println("Hai sbagliato! Ritenta.");
            }
            i++;
        }
    }
}
