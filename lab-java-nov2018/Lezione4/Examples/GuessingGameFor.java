import java.util.Scanner;

/**
 *
 * @author eric
 */
public class GuessingGameFor {
    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        int randInt = (int)(Math.random()*10)+1;
        
        //quale variabile; condizione; incremento
        for(int i = 0; i<3; i++){
            int in = scan.nextInt();//valore da tastiera inserito
            if(in!=randInt){
                System.out.println("Hai sbagliato!");
            }else{
                System.out.println("Hai vinto!");
                break;
            }            
            
            if(i==2){
                System.out.println("Il numero era "+randInt);
            }
        }
    
    
    }
    
    
}
