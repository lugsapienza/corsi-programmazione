


import java.util.Scanner;

/**
 *
 * @author eric
 */
public class StringExample {
    public static void main(String[] args) {
        /*Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        if(s=="prova"){
            System.out.println("La stringa inserita è uguale a \"prova\"");
        }*/
        
        String s = new String("prova");
        String s1 = new String("prova");
        String s2 = "test";
        String s3 = "test";
        System.out.println("prova".equals("prova"));
        System.out.println(s.equals(s1));
        System.out.println(s==s1);
        System.out.println(s2==s3);
        
        System.out.println("Stringa 1");
        System.out.println("Stringa 2");
        
        System.out.println("Stringa 1\nStringa2");
        
        System.out.println("Test\tTest");
        System.out.println("Test\btesst");
        System.out.println("String".charAt(3));
        
        System.out.println("Teste".indexOf("e"));
        String prova = "prova12311";
        System.out.println(prova.contains("va"));
        System.out.println(s2.equals("test"));
    }
}
