


/**
 *
 * @author eric
 */
public class WhileExample {
     public static void main(String[] args) {
        
        int index = 0;
        while(index<10){
            System.out.println("i = "+index);
            index++;
        }
         
    }
}
