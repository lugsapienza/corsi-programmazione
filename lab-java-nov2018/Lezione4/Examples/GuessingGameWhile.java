


import java.util.Scanner;


/**
 *
 * @author eric
 */
public class GuessingGameWhile {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int random = (int) (Math.random()*10)+1;    

        
        int tentativi = 2;
        while(input.nextInt()!=random){
            if(tentativi == 0){
                System.out.println("Hai sbagliato, hai perso");
                break;
            }else{
                System.out.println("Hai sbagliato");
            }
            tentativi--;
        }
        if(tentativi!=0){
            System.out.println("Hai vinto!");
        }
    
    }
}
