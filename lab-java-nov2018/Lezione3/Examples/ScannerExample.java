
import java.util.Scanner;

/**
 *
 * @author eric
 */

public class ScannerExample {
    
    public static void main(String[] args) {
        System.out.println("inizio");
        //Creo un oggetto scanner
        Scanner scan = new Scanner(System.in);
        
        //Leggo la stringa fino alla pressione invio     
        System.out.print("Inserisci una stringa: ");
        String linea = scan.nextLine();
        System.out.println("La stringa è: "+linea);
        
        //Leggo un intero
        System.out.print("Inserisci un intero: ");
        int line = scan.nextInt();
        System.out.println("L'intero è: "+line); 
        
        System.out.println("fine");
    }
    
}
