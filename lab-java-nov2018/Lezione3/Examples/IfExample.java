/**
 *
 * @author eric
 */
public class IfExample {
    public static void main(String[] args) {

        //Esempio 1
        //Eseguirà il codice all'interno del ramo if.
        System.out.println("Esempio 1");
        if(true){
            System.out.println("Ramo if");
        }
        
        System.out.println("------------------");


        //Esempio 2
        //Non esegiuirà il codice all'interno del ramo if.
        System.out.println("Esempio 2");
        if(false){
            System.out.println("Ramo if");
        }
        
        System.out.println("------------------");


        //Esempio 3
        //Eseguirà l'if ma non l'else
        System.out.println("Esempio 3");
        if(true){
            System.out.println("Ramo if");
        }else{
            System.out.println("Ramo else");
        }
        
        System.out.println("------------------");

        //Esempio 4
        //Eseguirà l'else e non l'if.
        System.out.println("Esempio 4");
        if(false){
            System.out.println("Ramo if");
        }else{
            System.out.println("Ramo else");
        }
        
    }
}
