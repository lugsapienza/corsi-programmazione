
import java.util.Random;



/**
 *
 * @author eric
 */
public class SwitchExample {
    public static void main(String[] args) {
        System.out.println("Inizio");
        String a = "22";
        switch(a){
            case "1":
                System.out.println("a vale 1");
                System.out.println("123");
                System.out.println("Altro");
                break;
            case 2+"2":
                System.out.println("a vale 2");
                break;
            case "3":
                System.out.println("a vale 3");
                break;
        }
        System.out.println("Fine");
        
    }
}
