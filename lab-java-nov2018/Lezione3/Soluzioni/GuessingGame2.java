import java.util.Scanner;

/**
 *
 * @author eric
 */
public class GuessingGame2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int random = (int) (Math.random()*10)+1;
        
        //Primo tentativo
        System.out.print("Inserisci un numero (3 tentativi): ");
        if(random!=scan.nextInt()){
            //Secondo tentativo
            System.out.println("Sbagliato!\nTentativi rimasti: 2");
            System.out.print("Inserisci un numero: ");
            if(random!=scan.nextInt()){
                //Terzo tentativo
                System.out.println("Sbagliato!\nTentativi rimasti: 1");
                System.out.print("Inserisci un numero: ");
                if(random!=scan.nextInt()){
                    System.out.println("Hai perso! Il numero era: "+random);
                }else{
                    System.out.println("Hai vinto!");
                }
            }else{
                System.out.println("Hai vinto!");
            }
        }else{
            System.out.println("Hai vinto!");
        }
    }
}
