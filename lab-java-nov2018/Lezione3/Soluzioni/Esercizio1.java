
import java.util.Scanner;

/**
 *
 * @author eric
 */
public class Esercizio1 {
    public static void main(String[] args) {
        //Creo l'oggetto scanner
        Scanner input = new Scanner(System.in);
        System.out.println("Questo programma calcola la somma di due interi e stampa a schermo se il valore è minore o maggiore uguale di 100.");
        
        //Prendo i due numeri in input da tastiera
        System.out.print("Inserisci un numero: ");
        int a = input.nextInt();
        System.out.print("Inserisci un altro numero: ");
        int b = input.nextInt();
        
        //Controllo se la somma è maggiore uguale o minore di 100
        if(a+b>=100){
            System.out.println("La somma è maggiore uguale di 100");
        }else{
            System.out.println("La somma è minore di 100");
        }
    }
}
