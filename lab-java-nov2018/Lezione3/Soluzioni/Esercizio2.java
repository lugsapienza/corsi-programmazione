
import java.util.Scanner;

/**
 *
 * @author eric
 */
public class Esercizio2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        //Leggo i due interi
        System.out.print("Inserisci un intero: ");
        int val1 = in.nextInt();
        
        System.out.print("Inserisci un secondo intero: ");
        int val2 = in.nextInt();
        
        //Pulisco il buffer per consumare il \n mancato dai nextInt()
        in.nextLine();
        
        //Leggo l'operatore
        System.out.print("Inserisci un operatore tra +, -, * o / : ");
        String operatore = in.nextLine();
        switch(operatore){
            case "+":
                System.out.println("Il risultato è: "+(val1+val2));
                break;
            case "-":
                System.out.println("Il risultato è: "+(val1-val2));
                break;
            case "*":
                System.out.println("Il risultato è: "+(val1*val2));
                break;
            case "/":
                System.out.println("Il risultato è: "+(val1/val2));
                break;
            default:
                System.out.println("Operatore non valido!");
        }
    }
}
