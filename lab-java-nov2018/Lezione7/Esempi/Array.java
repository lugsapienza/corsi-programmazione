import java.util.Arrays;

import static java.lang.System.out;
import static java.lang.System.err;

public class Array {
	public static void main(String[] args){
		int[] int_ar =  {0,1,2,3,4,5,6,7,8,9};  //Definire un array a partire da valori noti
		int[] int_ardef = new int[10]; //Definire un array con valori di default (0 tipi primitivi, NULL oggetti)
		out.println("size int_ar: " + int_ar.length + " size int_ardef: " + int_ardef.length +"\n");   //Stampa le lunghezze dei due array
		out.println(int_ar+ " " + int_ardef + "\n");  //Errore, non è possibile stampare così il contenuto di un array. 
for_st:		for(int i=0; i < int_ar.length; i++){  //Il metodo standard per iterare in un array
			    out.print(int_ar[i] + " "); //Stampa il numero alla posizione i dell'array
		    }
		
		out.print("\n");
for_en:		for(int i : int_ar){  //Iterare un array con un ForEach loop. 
		        out.print(i + " "); //Stampa l'elemento del passo corrente dell'iterazione
		    }
		
		out.print("\n");
		
incr_st:	for(int i=0; i < int_ar.length; i++){ 
			    int_ar[i] += 10;   //Modifica l'elemento di indice i nell'array.
		    }
for_st1:	for(int i=0; i < int_ar.length; i++){
			    out.print(int_ar[i] + " ");  //L'array viene modificato 
		    }
		out.print("\n");

incr_err:	for(int i : int_ar){
			    i+=2;  //Errore: In un forEach loop modificare il valore dell'iterazione non modifica il valore originale.
		    }
for_en:		for(int i : int_ar){
		     	out.print(i + " ");  //Infatti questa iterazione è uguale all'iterazione for_st1
		    }
		out.print("\n");
		

		//Test di eguaglianza farlocco
		int_ar = new int[]{0,1,2,3,4,5,6,7,8,9};
		int[] int_ar1 =  {0,1,2,3,4,5,6,7,8,9}; 
		out.println("int_ar == int_ar1:"+ (int_ar1 == int_ar));  // Nonostante int_ar ed int_ar1 abbiano lo stesso contenuto sono  due array diversi e  la comparazione fallisce
		out.println("int_ar1.equals(int_ar): " + int_ar1.equals(int_ar));  //Ed anche il metodo equals confronta solo se i due array siano lo stesso oggetto.  
		//Con gli  Array usare "==" o "equals" è la stessa cosa.
		
		/** Come fare quindi a confrontare realmente due array? Nel modo grezzo ci sono due passaggi:
		*   In primo luogo si deve confrontare la loro lunghezza. 
		*   Successivamente se sono di lunghezza uguale confrontarne gli elementi con un ciclo
		* NOTA: questo potrebbe essere un esercizio da fare per prendere dimestichezza con gli Array.
		*/
		
		/**
		* Tuttavia Java ci ha già pensato per noi. Infatti nella  classe Arrays ci sono molti metodi per aiutarci ad eseguire operazioni
		* con gli array. Ad esempio:
		* Confrontare per uguaglianza.
		* Ottenere una rappresentazione in stringa degli elementi.
		* Copiare un array.
		*/
		//Testare realmente l'uguaglianza di due array
		out.println("Arrays.equals(int_ar,int_ar1): " + Arrays.equals(int_ar,int_ar1));
		//Stampare il contenuto di un array:
		out.println("int_ar:" + Arrays.toString(int_ar));
		//Copiare un Array in un altro array:
		int[] copy = Arrays.copyOf(int_ar, int_ar.length);
		out.println("copy: " + Arrays.toString(copy));

		/**
		* La classe Arrays contiene numerose altre funzioni per operare sugli Array.
		*  Consultare la documentazione alla pagina:
		* https://docs.oracle.com/javase/10/docs/api/java/util/Arrays.html
		*/
		
		/**
		* C'è un caso d'uso particolare per gli Array.
		* Fino ad adesso abbiamo visto gli array monodimensionali. che rappresentano un vettore di elementi.
		* A volte avremmo necessità di rappresentare concetti non facilmente modellabili con un solo Array.
		* Ad esempio Matrici, rappresentabili come Array bidimensionali o Array di Array
		*/
		int[][] matrix = new int[10][10]; //Definisce un array di array di dimensione 10x10 con tutti 0
		int[][] mat1 = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};  //Definisce un array di array 4x4 con valori conosciuti
		int[][] mat2 = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		
		/**
		* Per gli array di array valgono le stesse cose degli array normali, ma con una profondità maggiore
		* Infatti ad esempio per accedere agli elementi di una matrice non basta più un semplice ciclo, ma ne servono due
		*/
		out.println("Matrix mat1");
		for(int[] arr: mat1){
			for(int i : arr){
				out.print(i + " ");
			}
			out.print("\n");
		}
		out.println("End matrix");
		/**
		* Ora iniziano i problemi. 
		* MA anche Arrays.equals e Arrays.toString non hanno probabilmente il significato che vogliamo.
		* Infatti entrambe le funzioni si fermano al primo livello degli array da scorrere, non entrando quindi in possibili sotto-array
		*/
		//Testare  l'uguaglianza di due array di array : Errore ritorna true solo se gli array interni sono condivisi a livello di riferimenti
		out.println("Matrix Arrays.equals(mat1,mat2): "+ Arrays.equals(mat2,mat1));
		//Stampare il contenuto di un array:Errore: questo stamperà gli indirizzi dei sotto array.
		out.println("mat1:" + Arrays.toString(mat1));
		//Copiare un Array di array in un altro array di array: l'array risultante avra i sotto-array condivisi con l'array di partenza.
		int[][] copy1 = Arrays.copyOf(mat1, mat1.length);
		Arrays.equals(copy1, mat1);
		/**
		* Fortunatamente la classe Arrays ci offre anche delle utilità per operare su array di array.
		* Questi metodi funzionano su array di più livelli
		*/

		//Testare  l'uguaglianza di due array di array, funziona per ogni livello
		out.println("Arrays.deepEquals(mat1,mat2): " + Arrays.deepEquals(mat2,mat1));
		//Stampare il contenuto di un array di array
		out.println("mat1:" + Arrays.deepToString(mat1));
		
	}
}


/**
1)Scrivere un programma che confronta due array stampando true se sono uguali, false altrimenti

2)Scrivere un programma che presa una matrice int[][], ne stampa il contenuto su più righe


ES:
int[][] = {{1,2,3},{1,2,3},{1,2,3}}


Ris:
1 2 3
1 2 3
1 2 3
*/























