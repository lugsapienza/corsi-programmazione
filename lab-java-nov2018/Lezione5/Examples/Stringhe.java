/**
 *
 * @author eric
 */
public class Stringhe {
    public static void main(String[] args) {
        //Stringa creata a tempo di compilazione
        String s1 = "Questa è una stringa";
        String s2 = "Questa è una stringa";
        String s3 = new String("Questa è una stringa");
        
        //Stringa creata a tempo di esecuzione (quando eseguo il programma)
        
        
        //Primo test equals()
        System.out.println("Test 1\nEsempio equals");
        System.out.println("s1==s2: " + (s1==s2));
        System.out.println("s1.equals(s2): "+s1.equals(s2));
        System.out.println("s1==s3: "+ (s1==s3));
        System.out.println("s1.equals(s3): "+s1.equals(s3));
        
        //Secondo test contains()
        
        String contiene = "Casa";
        String contenuto = "as";
        
        System.out.println("");
        System.out.println("Test 2\nEsempio contains");
        System.out.println("String contiene = \"Casa\";\n" +
        "String contenuto = \"as\";");
        System.out.println("contiene.contains(contenuto): "+ contiene.contains(contenuto));
        
        //Terzo test replace("", "");
        String preRimpiazzo = "Coso";
        String postRimpiazzo = preRimpiazzo.replace("o", "a");
        
        System.out.println("");
        System.out.println("Test 3\nEsempio replace");
        System.out.println("String preRimpiazzo = \"Coso\";\n" + "String postRimpiazzo = preRimpiazzo.replace(\"o\", \"a\");");
        System.out.println("daRimpiazzare.replace(\"o\",\"a\"): "+ postRimpiazzo);
        
        //Quarto test length()
        String lungo = "Questa è una stringa molto lunga con 49 caratteri";
        
        System.out.println("");
        System.out.println("Test 4\nEsempio length");
        System.out.println("String lungo = \"Questa è una stringa molto lunga con 49 caratteri\";");
        System.out.println("lungo.length(): "+ lungo.length());
        
        //Quinto test
        String prova = "aaaabcb";
        int risultato = prova.indexOf("b");
        System.out.println("");
        System.out.println("Test 5\nEsempio indexOf");
        System.out.println("String prova = \"abbc\";\n" +
        "int risultato = prova.indexOf(\"b\");");
        
        System.out.println("risultato: "+ risultato);
    }
}
 