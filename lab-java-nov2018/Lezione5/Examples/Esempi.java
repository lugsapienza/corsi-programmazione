
import java.util.Scanner;

public class Esempi {
    public static void main(String[] args) {
        // aggiunge un \n che fa in modo che la prossima
        //stampa vada alla riga successiva
        System.out.println("*");
        

        //NON aggiunge il carattere \n. Tutte le stampe successive
        //saranno effettuate sulla stessa riga.
        System.out.print("");
        
        
        Scanner scan = new Scanner(System.in);

        //NB USARE
        String numero = scan.nextLine();
        int intero = Integer.parseInt(numero);
        
        //Anziché
        int num = scan.nextInt();
        
        
        //Esempi su println e print.
        int i;
        System.out.println("Primo ciclo");
        for(i = 0; i<7; i++){
            System.out.println("*");
        }
        
        System.out.println("\n");
        System.out.println("Secondo ciclo");
        for(i=0; i<7;i++)
        System.out.print("*");
        
    }
}
