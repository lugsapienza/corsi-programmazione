<h1>Requisiti</h1>
Studiare i metodi applicabili su una stringa. Vedi: [link](Examples/Stringhe.java)
<br>
Esempio:<code>
public class Abc {
    public static void main(String args[]){
        String s="abc";
        String t="abc";
        String s1=new String("abc");
        String t1=new String("abc");
    }
}
</code>
<br>
<b>Disposizione delle variabili nella memoria</b><br>
![alt img](lab-java-nov2018/Lezione5/Resources/memory.jpeg)


<h1>Esercizio 1</h1>
Scrivere un programma che utilizzando un ciclo for, stampi i quadrati dei numeri pari da 4 a 10.<br>
OUTPUT:<br>
16 36 64 100

<h1>Esercizio 2</h1>
Scrivere un programma che prenda in input una stringa e ne stampi la lunghezza.<br>
<b>Esempio</b>:<br>
INPUT:<br>
"Esempio di stringa"<br><br>

OUTPUT:<br>
18


<h1>Esercizio 3</h1>
Scrivere un programma che preso in input una stringa, ne stampi la sua metà centrale.<br>
<b>Esempio</b>:<br>
INPUT:<br>
"Casa"<br>
OUTPUT:<br>
"as"<br>

<h1>Esercizio 4</h1>
Scrivere un programma che prenda in input una stringa e stampi la quantità di vocali e spazi bianchi.

<b>Esempio</b>:<br>
INPUT:<br>
"Esempio di stringa"

OUTPUT:<br>
Numero di vocali: 7<br>
Numero di spazi: 2

<h1>Esercizio 5</h1>
Scrivere un programma che preso un intero n in input, stampi l'n-esimo numero della sequenza di fibonacci. Vedi: [fibonacci](https://en.wikipedia.org/wiki/Fibonacci_number)