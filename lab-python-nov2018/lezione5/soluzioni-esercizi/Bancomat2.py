#Funzioni utili:
def isnumero(s):
	if not type(s) == str:return None
	if s == "": return False
	dotted=False
	for c in range(len(s)):
		if s[c] in ["0","1","2","3","4","5","6","7","8","9"]:continue
		elif s[c] == "-" and c == 0:continue
		elif s[c] == "+" and c == 0:continue
		elif s[c] == "." and not dotted:
			dotted=True
			continue
		else: return False
	return True

if __name__ == "__main__": 
	#Il simbolo: __name__, che contiene il nome del modulo corrente, viene inizializzato al valore "__main__" quando il modulo viene eseguito in modalità script.
	#E' quindi possibile controllarne il valore, per far eseguire o meno determinate istruzioni, a seconda della modalità di utilizzo del modulo(script o importazione).
	import Conto

	#Corpo del programma
	print("Transazione 1 - Inserire l’importo da depositare:")
	s=""
	while not isnumero(s): s=input(">>>")
	Conto.deposita(float(s))
	print("Saldo corrente: ", Conto.saldo())
	print("Transazione 2 - Inserire l’importo da prelevare:")
	s=input(">>> ")
	while not isnumero(s): s=input(">>>")
	Conto.preleva(float(s))
	print("Saldo corrente: ", Conto.saldo())
	print("\nTransazioni completate.")
