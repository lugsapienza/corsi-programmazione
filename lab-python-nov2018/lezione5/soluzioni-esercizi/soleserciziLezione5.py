"""
Implementare la funzione: “differenzaListe” in modo che prenda due liste in input, e ne restituisca una terza, 
ottenuta facendo la differenza tra quelle fornite in input.
Le liste fornite in input dovranno restare invariate.

Esempio di chiamata: differenzaListe([1,2,3],[2,3,4])
Valore ritornato: [1,4]
"""

def differenzaListe(l1,l2):
	def diff(l1,l2,fnd):
		for i in range(len(l1)):
			for j in range(len(l2)):
				if l1[i]==l2[j]:
					return l1[:i]+diff(l1[i:],l2[:j]+l2[j+1:],True)
			if fnd:
				return diff(l1[1:],l2,False)
		return l1+l2
	return diff(l1,l2,False)
#Questa prima soluzione sfrutta una tecnica d'utilizzo delle funzioni, chiamata: Ricorsione,
#che consiste nel chiamare una funzione, all'interno della sua stessa definizione (la funzione chiama se stessa),
#passandole dei parametri d'input differenti da quelli della chiamata precedente(calcolati da quelli iniziali), 
#simulando il comportamento di un ciclo (ad ogni chiamata, i dati, sui quali lavorerà la funzione, saranno diversi).
#Tutto ciò è possibile grazie all'esistenza di una tabella dei simboli diversa per ogni chiamata a funzione(anche se la stessa).



#Le successive tre soluzioni fanno uso di una funzione di libreria: l.remove(e), che si occupa di rimuovere un elemento e, da una lista l.
def differenzaListe2(l1,l2):
	la=l1[:]
	lb=l2[:]
	for e in la[:]:#il for itera sulla COPIA della lista: la (la[:]).
		if e in lb:
			la.remove(e)
			lb.remove(e)
	return la+lb
	
def differenzaListe3(l1,l2):
	l=[]
	for e in l1:
		if e not in l2: l+=[e]
		else: l2.remove(e)  #C'è un problema su questa riga, riguardante le specifiche della funzione.
	for e in l2:
		if e not in l1: l+=[e]
	return l

####
def differenzaListe4(l1,l2):
	for e in l1:
		if e in l2: l2.remove(e)
	for e in l2:
		if e in l1: l1.remove(e)
	return l1+l2
#### Perché, questa soluzione, non funziona?


## NB: Tutte le soluzioni, a questo esercizio, si basano sul seguente concetto: La modifica della lunghezza di una lista l, 
## all'interno di un costrutto che ne sta iterando gli elementi, comporta un'inconsistenza dell'iteratore in uso.
## In altre parole: Evitare di alterare un oggetto sequenza mutabile, se quest'ultimo viene direttamente utilizzato all'interno di un'iteratore come il for.
## In tal caso iterare sulla copia dell'oggetto in questione (o[:]).
"""
Implementare la funzione "condividono", che prenda in input due liste, 
e ne restituisca una contenente gli oggetti in comune tra le due fornite in input.

Esempio di chiamata: condividono([1,"mamma",2+3j,"papa"],[2,"mamma",4])
Valore ritornato: ["mamma"]
"""

def condividono(l1,l2):
	l=[]
	for e in l1:
		if e in l2: l+=[e]
	return l



"""
Implementare la funzione "parolaPiuLunga", che prenda in input una stringa ed un separatore(stringa di lunghezza 1)
, e restituisca una stringa contenente la parola più lunga, utilizzando il separatore fornito, come carattere per distinguere le parole.

Se non specificato, il separatore dovrà essere, per default, il carattere spazio:" ".

Se specificato con lunghezza >1, il separatore dovrà mantenere il valore di default.

In caso il testo contenga solo il separatore(stringa vuota in caso di separatore predefinito) o stringa nulla, la funzione dovrà restituire il valore: None.

In caso il testo contenga caratteri di separazione consecutivi, quest'ultimi NON dovranno essere considerati come parole differenti.

Nel caso in cui il separatore non comparisse nel testo, la funzione dovrà ritornare la stringa originale.


Esempio di chiamata: parolaPiuLunga("ciao mamma comestai?")
Valore ritornato: "comestai?"

Esempio di chiamata: parolaPiuLunga("ciao mamma comestai?","mm")
Valore ritornato: "comestai?"

Esempio di chiamata: parolaPiuLunga("ciao mamma comestai?","m")
Valore ritornato: "estai?"

Esempio di chiamata: parolaPiuLunga("")
Valore ritornato: None

Esempio di chiamata: parolaPiuLunga(" ")
Valore ritornato: None

Esempio di chiamata: parolaPiuLunga("m","m")
Valore ritornato: None

Esempio di chiamata: parolaPiuLunga("mamma", "m")
Valore ritornato: "a"

Esempio di chiamata: parolaPiuLunga("mamma", "p")
Valore ritornato: "mamma"
"""

def parolaPiuLunga(testo, sep=" "):
	if not type(sep) == str or len(sep)>1: sep=" "
	if not type(testo) == str or testo == "" or testo == sep: return None
	piulunga=""
	p=""
	lung=0
	for c in testo+sep:
		if not c == sep:p+=c
		elif not p == "" and len(p)>lung:
			lung=len(p)
			piulunga=p
			p=""
		else: p=""
	return piulunga
