#Funzioni utili:
def isnumero(s):
	if not type(s) == str:return None
	if s == "": return False
	dotted=False
	for c in range(len(s)):
		if s[c] in ["0","1","2","3","4","5","6","7","8","9"]:continue
		elif s[c] == "-" and c == 0:continue
		elif s[c] == "+" and c == 0:continue
		elif s[c] == "." and not dotted:
			dotted=True
			continue
		else: return False
	return True

if __name__ == "__main__": #Vogliamo eseguire il corpo, SOLO in modalità script.
	import Conto
	import sys #Serve per accedere al simbolo "argv"

	#Corpo del programma
	prmslen=len(sys.argv) #prmslen serve per non eseguire più volte la funzione len (la lunghezza di argv non può variare durante l'esecuzione)
	
	print("Transazione 1",end="") #end="" -> permette di NON adare a capo automaticamente(sovrascrive il carattere di terminazione '\n', aggiunto in automatico dalla print, come ultimo carattere da stampare), dopo aver stampato la stringa: "Transazione 1"
	s=""
	if prmslen >= 2 and isnumero(sys.argv[1]):  #Se sono stati inseriti almeno 2 parametri (nome modulo e valore da depositare), ed il secondo è un numero: usalo come valore da depositare.
		s=sys.argv[1]
		print("\n\nE' stato inserito l'importo: ",s,"\n")
	else: 										#Altrimenti eseguine l'acquisizione da tastiera.
		print(" - Inserire l’importo da depositare:")
		while not isnumero(s): s=input(">>>")
	Conto.deposita(float(s))
	print("Saldo corrente: ", Conto.saldo())
	
	
	print("Transazione 2",end="")
	s=""
	if prmslen >= 3 and isnumero(sys.argv[2]): #Se sono stati inseriti almeno 3 parametri (nome modulo, valore da depositare e valore da prelevare), ed il terzo è un numero: usalo come valore da prelevare.
		s=sys.argv[2]
		print("\n\nE' stato inserito l'importo: ",s,"\n")
	else:										#Altrimenti eseguine l'acquisizione da tastiera.
		print(" - Inserire l’importo da prelevare:") 
		while not isnumero(s): s=input(">>>")
	Conto.preleva(float(s))
	print("Saldo corrente: ", Conto.saldo())
	print("\nTransazioni completate.")
