"""
Implementare la funzione: “differenzaListe” in modo che prenda due liste in input, e ne restituisca una terza, 
ottenuta facendo la differenza tra quelle fornite in input.
Le liste fornite in input dovranno restare invariate.

Esempio di chiamata: differenzaListe([1,2,3],[2,3,4])
Valore ritornato: [1,4]
"""

def differenzaListe(l1,l2):pass



"""
Implementare la funzione "condividono", che prenda in input due liste, 
e ne restituisca una contenente gli oggetti in comune tra le due fornite in input.

Esempio di chiamata: condividono([1,"mamma",2+3j,"papa"],[2,"mamma",4])
Valore ritornato: ["mamma"]
"""

def condividono(l1,l2):pass



"""
Implementare la funzione "parolaPiuLunga", che prenda in input una stringa ed un separatore(stringa di lunghezza 1)
, e restituisca una stringa contenente la parola più lunga, utilizzando il separatore fornito, come carattere per distinguere le parole.

Se non specificato, il separatore dovrà essere, per default, il carattere spazio:" ".

Se specificato con lunghezza >1, il separatore dovrà mantenere il valore di default.

In caso il testo contenga solo il separatore(stringa vuota in caso di separatore predefinito) o stringa nulla, la funzione dovrà restituire il valore: None.

In caso il testo contenga caratteri di separazione consecutivi, quest'ultimi NON dovranno essere considerati come parole differenti.

Nel caso in cui il separatore non comparisse nel testo, la funzione dovrà ritornare la stringa originale.


Esempio di chiamata: parolaPiuLunga("ciao mamma comestai?")
Valore ritornato: "comestai?"

Esempio di chiamata: parolaPiuLunga("ciao mamma comestai?","mm")
Valore ritornato: "comestai?"

Esempio di chiamata: parolaPiuLunga("ciao mamma comestai?","m")
Valore ritornato: "estai?"

Esempio di chiamata: parolaPiuLunga("")
Valore ritornato: None

Esempio di chiamata: parolaPiuLunga(" ")
Valore ritornato: None

Esempio di chiamata: parolaPiuLunga("m","m")
Valore ritornato: None

Esempio di chiamata: parolaPiuLunga("mamma", "m")
Valore ritornato: "a"

Esempio di chiamata: parolaPiuLunga("mamma", "p")
Valore ritornato: "mamma"
"""

def parolaPiuLunga(testo, sep):pass




#NB: La parola: 'pass' serve a segnalare all'interprete che il blocco di codice in questione non fa nulla; rendendo possibile l'esecuzione del modulo, senza errori.
#SOSTITUIRE 'pass' con l'implementazione della funzione.
