#ESERCITAZIONE - LEZIONE 5

"""
TESTO:

Scrivere un modulo "doppio.py" che sia in grado di leggere due parametri numerici interi, passati alla sua esecuzione,
e di stampare a video il doppio di entrambi i valori numerici passati.



ESEMPIO D'ESECUZIONE:


Esecuzione: $python3 doppio.py 2 3

Output visivo:

Doppio di 2 = 4
Doppio di 3 = 6


"""

#PREAMBOLO ALLA SOLUZIONE:
"""
In questo programma bisogna eseguire il doppio di due parametri numerici interi, passati all'atto dell'esecuzione del modulo.
All'interno del modulo bisogna quindi prelevare tali parametri, per poterne eseguire la stampa del doppio.
Quando si esegue un modulo, quest'utlimo viene passato come parametro all'interprete:

Esempio: $python3 modulo.py



L'interprete è un programma che, come molti altri, 
può essere eseguito da un'interfaccia a linea di comando (Terminale(su sistemi Unix), Prompt dei comandi(su sistemi Windows)) 
passandogli dei parametri preceduti da uno spazio:

Sintassi d'esecuzione di un programma: [NomeProgramma]SPAZIO[parametro_1]SPAZIO[parametro_2]SPAZIO...SPAZIO[parametro_N]


Esempi: 
cd [directory]

Spostamento nella directory: "directory".


python [moduloDaEseguire(facoltativo)]

Esecuzione di un modulo python(se specificato come parametro) o dell'interprete interattivo.




L'interprete di python può prendere diversi parametri d'input, il primo è quello dell'eventuale modulo da eseguire, 
gli eventuali altri parametri possono essere quelli richiesti da uno specifico modulo o da un'istruzione che si vuole eseguire in modalità interattiva.

Esempio:

Se volessimo eseguire un modulo di nome: "stampa.py", che prende in input, all'esecuzione, una stringa "ciao", e la stampa a video, 
dovremmo scrivere:

$python3 stampa.py ciao

Se invece volessimo eseguire l'interprete in modalità interattiva(quindi senza specificare il nome di un modulo), 
passandogli, anche in questo caso, la stringa "ciao", dovremmo scrivere:

$python3 - ciao


Dove il trattino: - viene utilizzato per dire che non si vuole eseguire nessun modulo(il primo parametro viene trattato sempre come fosse il nome di un modulo), 
ma solo passare i parametri all'interprete.
In tal caso la stringa "ciao" potrà essere utilizzata, nell'interprete interattivo, per una qualsiasi computazione.


MA COM'E' POSSIBILE LEGGERE I PARAMETRI PASSATI ALL'INTERPRETE ALL'ATTO DELLA CHIAMATA? 


Il modulo: sys definisce una serie di simboli utili alla comunicazione con l'interprete; tra questi vi è la lista: argv 
che contiene tutti i parametri che sono stati passati all'interprete(nel caso della nostra esercitazione: doppio.py, 2 e 3).

E' quindi possibile accedere ai parametri di nostro interesse, importando il modulo sys e prelevando quest'ultimi da argv, nel seguente modo: sys.argv[I],
dove I è l'i-esimo parametro passato all'interprete, contato a partire da 0.
L'accesso ai parametri dell'interprete è possibile sia all'interno di un modulo, che in modalità interattiva.

NB: Tenere presente che argv è una lista che contiene tutti gli elementi, AUTOMATICAMENTE RAPPRESENTATI COME STRINGHE, passati all'interprete alla sua escuzione; 
ciò vuol dire che, se quest'ultimo venisse chiamato con due parametri: il modulo (doppio.py) ed una stringa ("ciao"), argv avrebbe lunghezza 2 e 
conterrebbe le stringhe: 'doppio.py' e 'ciao'.


Esempio:

$python3 doppio.py ciao  -> ciao è stato specificato senza gli apici("ciao"), ma verrà comunque trattato come una stringa.

argv: ['doppio.py','ciao']

argv è quindi una lista di SOLE STRINGHE!



Accertarsi dell'effettiva presenza, in argv, di un parametro, prima di utilizzarlo:

$python3 miomodulo.py ciao -> produrrà il seguente argv: ['miomodulo.py','ciao'] (argv[0] vale: 'miomodulo.py' e argv[1] vale: 'ciao').

Se in miomodulo.py venisse eseguita la seguente istruzione: sys.argv[2], 
il programma andrebbe in errore, stampando: "IndexError: list index out of range".

"""


#SOLUZIONE:


def isnumero(s):
	if not type(s) == str:return None    #Se non viene passata una stringa, la funzione termina restituendo valore nullo(il suo codice non ha senso se applicato ad altri tipi di oggetti). 
	if s == "": return False    #Una stringa vuota non rappresenta un numero; si ritorna subito: False.
	dotted=False    #Utilizzata per tenere traccia della presenza di un punto(che denota un numero decimale) all'interno della stringa.
	for c in range(len(s)):    #Si utilizza il range, perché nei successivi controlli vengono utilizzati gli indici degli elementi della stringa.
		if s[c] in ["0","1","2","3","4","5","6","7","8","9"]:continue    #Se è un numero, avalizza il prossimo carattere.
		elif s[c] == "-" and c == 0:continue    #Se il primo carattere è il segno: +(numero positivo), passa al prossimo.
		elif s[c] == "+" and c == 0:continue    #Se il primo carattere è il segno: -(numero negativo), passa al prossimo.
		elif s[c] == "." and not dotted:    #Se il carattere è il . e non ne sono stati trovati altri fin'ora, passa al prossimo carattere.
			dotted=True
			continue
		else: return False    #In tutti gli altri casi il carattere non'è valido e quindi la stringa non rappresenta un numero valido.
	return True    #Se sono stati esaminati tutti i caratteri, allora la stringa è un numero valido.

#Questa funzione viene definita, come supporto al programma, per verificare che una data stringa rappresenti un numero(reale positivo o negativo).




import sys #Viene importato il modulo di sistema per poter accedere alla lista argv contenente i parametri passati all'interprete(stringhe).

if len(sys.argv) == 3: #Viene controllato che i parametri passati alla chiamata siano 3.
	if isnumero(sys.argv[1]) and isnumero(sys.argv[2]): #Se entrambi i parametri d'interesse sono interi(verificati tramite la funzione sopra definita), ne viene eseguita la stampa del doppio.
		print("Doppio di argv[1]: ",int(sys.argv[1])*2)
		print("Doppio di argv[2]: ",int(sys.argv[2])*2)

#In caso cotrario il programma terminerà senza fare nulla.


"""
NOTE: 


L'istruzione: continue , se utilizzata all'interno di un ciclo, permette di passare all'iterazione successiva, saltando le eventuali istruzioni a seguire. 

Esempio:

l=[1,2,3]
for e in l:
	continue
	print(e)
	

Questo codice non eseguirà mai la print.
"""
