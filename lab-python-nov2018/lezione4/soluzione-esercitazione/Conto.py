"""
Il modulo Conto, che rappresenta il conto in banca, deve mettere a disposizione degli altri moduli le tre operazioni che è possibile eseguire su un conto, implementando le funzioni:
	•	preleva(quantita): prende in input una quantità di tipo float ed aggiorna il valore del conto, sottraendovi tale quantità.
	•	deposita(quantita): prende in input una quantità di tipo float ed aggiorna il valore del conto, aggiungendovi tale quantità.
	•	saldo(): restituisce il valore attuale del conto.
Per tenere traccia dei soldi presenti nel conto, il modulo dovrà inoltre definire il simbolo: giacenza, associato ad un valore float (variabile float), che verrà letto o scritto dalle funzioni sopra definite.
"""


giacenza=0.

def preleva(q):
	global giacenza
	giacenza -=q

def deposita(q):
	global giacenza
	giacenza +=q

def saldo():
	global giacenza
	return giacenza
