"""
TESTO ESERCITAZIONE:

Transazione 1:
	⁃	Stampa a video del messaggio: “Transazione 1 - Inserire l’importo da depositare:”.
	⁃	Acquisizione di un valore numerico float da tastiera.
	⁃	Chiamata al servizio di deposito somme, messo a disposizione dal modulo Conto(funzione: deposita(quantità)), con la specifica della quantità, acquista da tastiera (opportunamente convertita).
	⁃	Stampa a video del messaggio: “Saldo corrente: ”, seguito dal saldo del conto, prelevato tramite il servizio: saldo(), messo a disposizione dal modulo Conto.

Transazione 2:
	⁃	Stampa a video del messaggio: “Transazione 2 - Inserire l’importo da prelevare:”.
	⁃	Acquisizione di un valore numerico float, da tastiera.
	⁃	Chiamata al servizio di prelievo somme, messo a disposizione dal modulo Conto(funzione: preleva(quantità)), con la specifica della quantità, acquista da tastiera (opportunamente convertita).
	⁃	Stampa a video del messaggio: “Saldo corrente: ”, seguito dal saldo del conto, prelevato tramite il servizio: saldo(), messo a disposizione dal modulo Conto.
Stampa a video del messaggio: “Transazioni completate.” e terminazione del programma.
"""
import Conto


#Funzioni utili:
def isnumero(s):
	if not type(s) == str:return None
	if s == "": return False
	dotted=False
	for c in range(len(s)):
		if s[c] in ["0","1","2","3","4","5","6","7","8","9"]:continue
		elif s[c] == "-" and c == 0:continue
		elif s[c] == "+" and c == 0:continue
		elif s[c] == "." and not dotted:
			dotted=True
			continue
		else: return False
	return True


#Corpo del programma
print("Transazione 1 - Inserire l’importo da depositare:")
s=""
while not isnumero(s): s=input(">>>")
Conto.deposita(float(s))
print("Saldo corrente: ", Conto.saldo())
print("Transazione 2 - Inserire l’importo da prelevare:")
s=input(">>> ")
while not isnumero(s): s=input(">>>")
Conto.preleva(float(s))
print("Saldo corrente: ", Conto.saldo())
print("\nTransazioni completate.")


"""
NOTE:

Questo file contiene delle righe che iniziano con il carattere: #, seguito 
da testo in linguaggio naturale.
Quest'ultime sono commenti, cioè sequenze di caratteri che iniziano con
il carattere: # e vengono ignorate dall'interprete.
Sono utili per rendere il codice comprensibile, aggiungendo appunti o spiegazioni dove utile.


In python esistono le stringhe multiriga; dichiarabili utilizzando tre virgolette all'inizio 
e tre alla fine, invece di una singola o una doppia.
Le stringhe multiriga sono utili anche per inserire commenti su più righe, come in questo caso.


E' possibile passare una stringa alla funzione input(), che verrà visualizzata
sulla riga d'inserimento.


Per evitare di passare un valore non numerico alla funzione float(), 
è stata definita una funzione supplementare: isnumero(), 
che prende la stringa restituita da input(), e verifica che essa rappresenti 
effettivamente un numero.
Leggere l'implementazione di tale funzione e cercare di capirne il funzionamento.


Che scopo ha l'istruzione alla riga 38? E che differenza c'è con quella alla riga 43?	
"""
