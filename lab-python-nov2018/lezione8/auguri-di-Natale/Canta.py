"""
LEGGERE IL FILE "LEGGI-PRIMA-QUESTO-FILE.TXT" CONTENENTE LE ISTRUZIONI.
"""

from Suoni import Nota as note
from Suoni import suona
import time
import sys

#Funzioni utili
def leggi_note(nomefile,sep="\n"):
	if not type(nomefile) == str or nomefile == "": return []
	note=[]
	try:
		f=open(nomefile)
		note=f.read().split(sep)
		f.close()
	except Exception: return []
	return note
"""
leggi_note è una funzione che prende in input il nome di un file ed, eventualmente, una stringa di separazione, 
e restituisce una lista contenente tutte le note presenti nel file, utilizzando la stringa di separazione 
per distinguere le note.
"""



immagine="""
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM,MMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMWd,,dWMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMWOc,.  .,cOWMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMNo.    .oNMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMO;;cc;;OMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMNX0kkKXNMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMKo.  .oXMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMXo.      .oXMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMXo.          .oKMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMWXo.              .oXMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMXo.                  .oXMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMNx,.                    .,xNMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMNKkl.                  .lO0NMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMWO;.                     :OWMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMWO:.                        .:OWMMMMMMMMMMMMMM
MMMMMMMMMMMMNO:.                            .:OWMMMMMMMMMMMM
MMMMMMMMMMMMNkdddo'                      'odddkNMMMMMMMMMMMM
MMMMMMMMMMMMMMMNk;                        ;kNMMMMMMMMMMMMMMM
MMMMMMMMMMMMMNk;                            ;kNMMMMMMMMMMMMM
MMMMMMMMMMMNk;                                ;kNMMMMMMMMMMM
MMMMMMMMMNk;                                    ;kNMMMMMMMMM
MMMMMMMMMKdlllll,                          ,llllldKWMMMMMMMM
MMMMMMMMMMMMMW0c.                          .c0WMMMMMMMMMMMMM
MMMMMMMMMMMW0c.                              .c0WMMMMMMMMMMM
MMMMMMMMMW0c.                                  .c0WMMMMMMMMM
MMMMMMMWOc.                                      .c0WMMMMMMM
MMMMMMMXxooooooooooooooooo,      ,oolloooooooooooooxXMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMWl      cWMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMM0,      '0MMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMx.      .dMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMWc        lWMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
"""
testo="""
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::BUON:NATALE:DAL:LUG:SAPIENZA:_::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
"""

#Corpo del programma
if len(sys.argv) == 2:
	#Leggi note dal file specificato
	canzone=leggi_note(sys.argv[1])
	print(immagine)
	#Suona le note
	for nota in canzone:
		if nota=="":time.sleep(0.6) #Le pause, tra una o più note, vengono rappresentate mediante una stringa vuota(riga vuota all'interno del file). In caso di pause, il programma non farà nulla per 0.6 secondi (time.sleep()).
		else: 
			if suona(note[nota],550): #Questa funzione ritorna un codice d'errore, diverso da 0, nel caso in cui l'emissione del suono non dovesse andare a buon fine. 
				print("\n!! Please, install 'sox' to execute this module on Unix systems !!\n   Try with: sudo apt install sox\n")
				break

print(testo)
