from os import name as osversion
from os import system
if osversion == "nt": import winsound as ws

#Definizione note musicali
Nota = {"DO": 262, "DOd": 277, "RE": 294,"REd": 311, "MI": 330, "FA": 349, "FAd": 370, "SOL": 392, "SOLd": 415, "LA": 440, "LAd": 466, "SI": 494}
Durata = {"INTERO": 1600, "MEZZO": 800, "QUARTO": 400, "OTTAVO": 200, "SEDICESIMO": 100}

#Definizione funzionalità
def suona(nota, durata):
	if osversion == "nt": return ws.Beep(nota,durata)
	elif osversion == "posix": return system('play --no-show-progress --null --channels 1 synth %s sine %f' % (durata/1000, nota))
