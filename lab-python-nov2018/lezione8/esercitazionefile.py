"""
Scrivere un programma che, dati in input (come parametri alla chiamata) il nome di un file, ed una stringa e scriva la stringa all'interno del file specificato.
Nel caso la stringa non dovessere essere inserita, il programma dovrà stampare a video il contenuto del file specificato.
"""

import sys

paramcount = len(sys.argv)

if paramcount == 3:
	f=open(sys.argv[1],"w") #Apertura del file in modalità scrittura(a partire dall'inizio del file).
	print(sys.argv[2],file=f) #Scrittura della stringa passata come parametro, nel file f, partendo dall'inizio.
	print("Stringa: '",sys.argv[2],"' inserita nel file: '",sys.argv[1],"'",sep="") #sep="" viene utilizzato per non far inserire, alla print, gli spazi tra un parametro e l'altro.
	f.close() #Chiusura del file(bisogna sempre eseguira quando non serve più).
elif paramcount == 2:
	f=open(sys.argv[1]) #Apertura del file in modalità lettura.
	print(f.read()) #Lettura e stampa del contenuto del file.
	f.close()
else: print("\nUtilizzo: esercitazionefile.py <nomefile> [<stringa>]\n")

"""
NOTE:

La print() permette di stampare la rappresentazione testuale di un oggetto.
La stampa avviene, per default, a video, ma può anche essere reindirizzata ad un file 
('file=f' dove f è il simbolo associato al file, tramite la funzione open()).
E' possibile utilizzare il file: aprimi.txt, per testare la suddetta soluzione.

"""
